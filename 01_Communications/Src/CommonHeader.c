/**
  ******************************************************************************
  * @file    CommonHeader.c 
  * @author  HU Yaoyu <huyaoyu@sjtu.edu.cn>
  * @version V0.1
  * @date    24-November-2015
  * @brief   No brief for now
  ******************************************************************************
  * @attention
  *
	* No attention for now.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "CommonHeader.h"
#include <stdio.h> /* strng operations */
#include <string.h>
#include <stdlib.h>

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/* exported global variable */

ProjectDebugStringNode_t* projectDSN  = NULL;
ProjectDebugStringNode_t* currentPDSN = NULL;

/* Implementation ----------------------------------------------------------- */

int8_t isProjectBufferValid(ProjectBuffer_t* projBuffer)
{
	int8_t sta = PROJECT_FAIL;
	
	if (projBuffer == NULL)
	{
		return sta;
	}
	else
	{
		if (projBuffer->len <= 0)
		{
			return sta;
		}
		
		if (projBuffer->contentLen > projBuffer->len)
		{
			return sta;
		}
	}
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t clearProjectBuffer(ProjectBuffer_t* projBuffer)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	uint32_t i;
	
	/* check the argument */
	
	if (NULL == projBuffer)
	{
		/* error */
		return sta;
	}
	
	if (PROJECT_OK != isProjectBufferValid(projBuffer))
	{
		/* error */
		return sta;
	}
	
	/* argument check done */
	
	for (i=0;i< (projBuffer->len);i++)
	{
		*(projBuffer->buffer + i) = 0xFF;
	}
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t createDebugStringNode(ProjectDebugStringNode_t** pNodePointer)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	/* initial argument check */
	
	if (NULL == pNodePointer)
	{
		/* error */
		return sta;
	}
	
	if (NULL != *(pNodePointer))
	{
		/* error */
		return sta;
	}
	
	*(pNodePointer) = (ProjectDebugStringNode_t*)malloc(sizeof(ProjectDebugStringNode_t));
	if (NULL == *(pNodePointer))
	{
		return sta;
	}
	
	(*(pNodePointer))->next = NULL;
	
	sta = PROJECT_OK;
	
	return sta;
}


ProjectStatus_t fillDebugStringNode( ProjectDebugStringNode_t* pNode,
                                       const char* str,
																			 const char* file,
																			 int line,
																			 const char* func)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	uint32_t strLen  = 0; /* the length of the input string */
	uint32_t fileLen = 0; /* the length of the file-name string */
	uint32_t lineLen = 0; /* the length if the line-number string */
	uint32_t funcLen = 0; /* the length of the function-name string */
	
	/* initial input argument check */
	
	if ( NULL == pNode|| 
		   NULL == str  ||
	     NULL == file ||
	     NULL == func )
	{
		/* error */
		return sta;
	}
	
	if (100000 < line)
	{
		/* error */
		return sta;
	}
	
	if (NULL != pNode->debugString)
	{
		/* error */
		return sta;
	}
	
	
	/* initial input argument check done */
	
	strLen  = strlen(str);
	fileLen = strlen(file);
	lineLen = 6;
	funcLen = strlen(file);
	
	/* allocate memory */
	
	pNode->debugString = (char*)malloc(sizeof(char) * (strLen + fileLen + lineLen + funcLen + 5));
	if (NULL == pNode->debugString)
	{
		/* memory error */
		return sta;
	}
	
	sprintf(pNode->debugString,"%s: %6d(%s)\n%s", file, line, func, str);
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t appendDebugStringNode(ProjectDebugStringNode_t** current, ProjectDebugStringNode_t* newOne)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	ProjectDebugStringNode_t* p = NULL;
	
	/* initial input argument check */
	
	if (NULL == current || NULL == newOne )
	{
		/* error */
		return sta;
	}
	
	p = *current;
	
	if (NULL == p)
	{
		/* error */
		return sta;
	}
	
	if (NULL != p->next)
	{
		/* error */
		return sta;
	}
	
	/* initial argument check done */
	
	p->next = newOne;
	
	*(current) = newOne;
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t releaseDebugStringStack(ProjectDebugStringNode_t** head)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	ProjectDebugStringNode_t* p = NULL;
	
	/* initial argument check */
	
	if (NULL == head)
	{
		/* error */
		return sta;
	}
	
	p = *(head);
	
	if (NULL != p->next)
	{
		if ( PROJECT_OK != releaseDebugStringStack(&(p->next)) )
		{
			return sta;
		}
	}
	free(p->debugString);
	p->next = NULL;
	free(p);
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t composeDebugString(ProjectDebugStringNode_t* head, char* buffer, uint32_t len)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	ProjectDebugStringNode_t* node = NULL;
	
	uint32_t pos       = 0; /* pointing to the current available position */
	uint32_t strLen    = 0; /* the length of a string */
	uint8_t tailStrLen = 2; /* the length of the string suffix */
	char tailChar      = '\n';
	
	/* initial argument check */
	
	if (NULL == head || NULL == buffer)
	{
		/* error */
		return sta;
	}
	
	node = (head);
	
	if (NULL == node)
	{
		/* error */
		return sta;
	}
	
	if ( len == 0 )
	{
		/* error */
		return sta;
	}
	
	/* initial argument check done */
	
	do
	{
		/* obtain the length of the next string */
		strLen = strlen(node->debugString);
		
		/* check the remained capacity of the buffer */
		if (pos + strLen + tailStrLen > len)
		{
			if (0 != pos)
			{
				/* put a question mark before the last tailChar */
			
				*(buffer + pos - 2) = '?';
				*(buffer + pos - 1) = tailChar;
				*(buffer + pos)     = '\0';
			}
			break;
		}
		
		/* copy the debug string into the buffer */
		sprintf(buffer+pos,"%s%c%c",node->debugString,tailChar,tailChar);
		
		/* shift the pos variable */
		
		pos += (strLen + tailStrLen);
		
		node = node->next;
		
	}while(NULL != node || len == pos);
	
	if (0 == pos)
	{
		sta = PROJECT_FAIL;
	}
	else
	{
		sta = PROJECT_OK;
	}
	
	return sta;
}
