/**
  ******************************************************************************
  * @file    stm32f4xx_hal_msp.c
  * @author  MCD Application Team + Yaoyu HU <huyaoyu@sjtu.edu.cn>
  * @version V1.2.3
  * @date    09-October-2015
  * @brief   HAL MSP module.    
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************  
  */ 

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/** @addtogroup STM32F4xx_HAL_Examples
  * @{
  */

/** @defgroup UART_TwoBoards_ComDMA
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* UART handler declared in "main.c" file */
extern UART_HandleTypeDef llUartHandle;
extern UART_HandleTypeDef clUartHandle;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/** @defgroup HAL_MSP_Private_Functions
  * @{
  */

/**
  * @brief UART MSP Initialization for the low level hardware
  *        This function configures the hardware resources used in this project: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void initProjectUSARTForLowLevelHardware(UART_HandleTypeDef *huart)
{
	static DMA_HandleTypeDef hdma_tx;
  static DMA_HandleTypeDef hdma_rx;
  
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  USART_LL_TX_GPIO_CLK_ENABLE();
  USART_LL_RX_GPIO_CLK_ENABLE();
  /* Enable USART2 clock */
  USART_LL_CLK_ENABLE(); 
  /* Enable DMA1 clock */
  DMA_LL_CLK_ENABLE();   
  
  /*##-2- Configure peripheral GPIO ##########################################*/  
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USART_LL_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = USART_LL_TX_AF;
  
  HAL_GPIO_Init(USART_LL_TX_GPIO_PORT, &GPIO_InitStruct);
    
  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USART_LL_RX_PIN;
  GPIO_InitStruct.Alternate = USART_LL_RX_AF;
    
  HAL_GPIO_Init(USART_LL_RX_GPIO_PORT, &GPIO_InitStruct);
    
  /*##-3- Configure the DMA streams ##########################################*/
  /* Configure the DMA handler for Transmission process */
  hdma_tx.Instance                 = USART_LL_TX_DMA_STREAM;
  
  hdma_tx.Init.Channel             = USART_LL_TX_DMA_CHANNEL;
  hdma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
  hdma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdma_tx.Init.MemInc              = DMA_MINC_ENABLE;
  hdma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdma_tx.Init.Mode                = DMA_NORMAL;
  hdma_tx.Init.Priority            = DMA_PRIORITY_LOW;
  hdma_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
  hdma_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdma_tx.Init.MemBurst            = DMA_MBURST_INC4;
  hdma_tx.Init.PeriphBurst         = DMA_PBURST_INC4;
  
  HAL_DMA_Init(&hdma_tx);   
  
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmatx, hdma_tx);
    
  /* Configure the DMA handler for Transmission process */
  hdma_rx.Instance                 = USART_LL_RX_DMA_STREAM;
  
  hdma_rx.Init.Channel             = USART_LL_RX_DMA_CHANNEL;
  hdma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  hdma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdma_rx.Init.MemInc              = DMA_MINC_ENABLE;
  hdma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdma_rx.Init.Mode                = DMA_NORMAL;
  hdma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
  hdma_rx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;         
  hdma_rx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdma_rx.Init.MemBurst            = DMA_MBURST_INC4;
  hdma_rx.Init.PeriphBurst         = DMA_PBURST_INC4; 

  HAL_DMA_Init(&hdma_rx);
    
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmarx, hdma_rx);
    
  /*##-4- Configure the NVIC for DMA #########################################*/
  /* NVIC configuration for DMA transfer complete interrupt (USART_LL_TX) */
  HAL_NVIC_SetPriority(USART_LL_DMA_TX_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(USART_LL_DMA_TX_IRQn);
    
  /* NVIC configuration for DMA transfer complete interrupt (USART_LL_RX) */
  HAL_NVIC_SetPriority(USART_LL_DMA_RX_IRQn, 0, 0);   
  HAL_NVIC_EnableIRQ(USART_LL_DMA_RX_IRQn);
  
  /* NVIC configuration for USART TC interrupt */
  HAL_NVIC_SetPriority(USART_LL_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART_LL_IRQn);
}

/**
  * @brief UART MSP Initialization for the motors
  *        This function configures the hardware resources used in this project: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void initProjectUSARTForCommandLine(UART_HandleTypeDef *huart)
{
	static DMA_HandleTypeDef hcl_dma_tx;
  static DMA_HandleTypeDef hcl_dma_rx;
  
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  USART_CL_TX_GPIO_CLK_ENABLE();
  USART_CL_RX_GPIO_CLK_ENABLE();
  /* Enable USART clock */
  USART_CL_CLK_ENABLE(); 
  /* Enable DMA clock */
  DMA_CL_CLK_ENABLE();   
  
  /*##-2- Configure peripheral GPIO ##########################################*/  
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USART_CL_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = USART_CL_TX_AF;
  
  HAL_GPIO_Init(USART_CL_TX_GPIO_PORT, &GPIO_InitStruct);
    
  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USART_CL_RX_PIN;
  GPIO_InitStruct.Alternate = USART_CL_RX_AF;
    
  HAL_GPIO_Init(USART_CL_RX_GPIO_PORT, &GPIO_InitStruct);
    
  /*##-3- Configure the DMA streams ##########################################*/
  /* Configure the DMA handler for Transmission process */
  hcl_dma_tx.Instance                 = USART_CL_TX_DMA_STREAM;
  
  hcl_dma_tx.Init.Channel             = USART_CL_TX_DMA_CHANNEL;
  hcl_dma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
  hcl_dma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hcl_dma_tx.Init.MemInc              = DMA_MINC_ENABLE;
  hcl_dma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hcl_dma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hcl_dma_tx.Init.Mode                = DMA_NORMAL;
  hcl_dma_tx.Init.Priority            = DMA_PRIORITY_LOW;
  hcl_dma_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
  hcl_dma_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hcl_dma_tx.Init.MemBurst            = DMA_MBURST_INC4;
  hcl_dma_tx.Init.PeriphBurst         = DMA_PBURST_INC4;
  
  HAL_DMA_Init(&hcl_dma_tx);   
  
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmatx, hcl_dma_tx);
    
  /* Configure the DMA handler for Transmission process */
  hcl_dma_rx.Instance                 = USART_CL_RX_DMA_STREAM;
  
  hcl_dma_rx.Init.Channel             = USART_CL_RX_DMA_CHANNEL;
  hcl_dma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  hcl_dma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hcl_dma_rx.Init.MemInc              = DMA_MINC_ENABLE;
  hcl_dma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hcl_dma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hcl_dma_rx.Init.Mode                = DMA_NORMAL;
  hcl_dma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
  hcl_dma_rx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;         
  hcl_dma_rx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hcl_dma_rx.Init.MemBurst            = DMA_MBURST_INC4;
  hcl_dma_rx.Init.PeriphBurst         = DMA_PBURST_INC4; 

  HAL_DMA_Init(&hcl_dma_rx);
    
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmarx, hcl_dma_rx);
    
  /*##-4- Configure the NVIC for DMA #########################################*/
  /* NVIC configuration for DMA transfer complete interrupt (USART_CL_TX) */
  HAL_NVIC_SetPriority(USART_CL_DMA_TX_IRQn, 0, 1);
  HAL_NVIC_EnableIRQ(USART_CL_DMA_TX_IRQn);
    
  /* NVIC configuration for DMA transfer complete interrupt (USART_CL_RX) */
  HAL_NVIC_SetPriority(USART_CL_DMA_RX_IRQn, 0, 0);   
  HAL_NVIC_EnableIRQ(USART_CL_DMA_RX_IRQn);
  
  /* NVIC configuration for USART TC interrupt */
  HAL_NVIC_SetPriority(USART_CL_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(USART_CL_IRQn);
}

/**
  * @brief UART MSP Initialization for the debug-print port
  *        This function configures the hardware resources used in this project: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void initProjectUSARTForDebugPrint(UART_HandleTypeDef *huart)
{
	static DMA_HandleTypeDef hdp_dma_tx;
  static DMA_HandleTypeDef hdp_dma_rx;
  
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* Enable GPIO TX/RX clock */
  USART_DP_TX_GPIO_CLK_ENABLE();
  USART_DP_RX_GPIO_CLK_ENABLE();
  /* Enable USART clock */
  USART_DP_CLK_ENABLE(); 
  /* Enable DMA clock */
  DMA_DP_CLK_ENABLE();   
  
  /*##-2- Configure peripheral GPIO ##########################################*/  
  /* UART TX GPIO pin configuration  */
  GPIO_InitStruct.Pin       = USART_DP_TX_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = USART_DP_TX_AF;
  
  HAL_GPIO_Init(USART_DP_TX_GPIO_PORT, &GPIO_InitStruct);
    
  /* UART RX GPIO pin configuration  */
  GPIO_InitStruct.Pin = USART_DP_RX_PIN;
  GPIO_InitStruct.Alternate = USART_DP_RX_AF;
    
  HAL_GPIO_Init(USART_DP_RX_GPIO_PORT, &GPIO_InitStruct);
    
  /*##-3- Configure the DMA streams ##########################################*/
  /* Configure the DMA handler for Transmission process */
  hdp_dma_tx.Instance                 = USART_DP_TX_DMA_STREAM;
  
  hdp_dma_tx.Init.Channel             = USART_DP_TX_DMA_CHANNEL;
  hdp_dma_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
  hdp_dma_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdp_dma_tx.Init.MemInc              = DMA_MINC_ENABLE;
  hdp_dma_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdp_dma_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdp_dma_tx.Init.Mode                = DMA_NORMAL;
  hdp_dma_tx.Init.Priority            = DMA_PRIORITY_LOW;
  hdp_dma_tx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;
  hdp_dma_tx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdp_dma_tx.Init.MemBurst            = DMA_MBURST_INC4;
  hdp_dma_tx.Init.PeriphBurst         = DMA_PBURST_INC4;
  
  HAL_DMA_Init(&hdp_dma_tx);   
  
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmatx, hdp_dma_tx);
    
  /* Configure the DMA handler for Transmission process */
  hdp_dma_rx.Instance                 = USART_DP_RX_DMA_STREAM;
  
  hdp_dma_rx.Init.Channel             = USART_DP_RX_DMA_CHANNEL;
  hdp_dma_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
  hdp_dma_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
  hdp_dma_rx.Init.MemInc              = DMA_MINC_ENABLE;
  hdp_dma_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
  hdp_dma_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
  hdp_dma_rx.Init.Mode                = DMA_NORMAL;
  hdp_dma_rx.Init.Priority            = DMA_PRIORITY_HIGH;
  hdp_dma_rx.Init.FIFOMode            = DMA_FIFOMODE_DISABLE;         
  hdp_dma_rx.Init.FIFOThreshold       = DMA_FIFO_THRESHOLD_FULL;
  hdp_dma_rx.Init.MemBurst            = DMA_MBURST_INC4;
  hdp_dma_rx.Init.PeriphBurst         = DMA_PBURST_INC4; 

  HAL_DMA_Init(&hdp_dma_rx);
    
  /* Associate the initialized DMA handle to the the UART handle */
  __HAL_LINKDMA(huart, hdmarx, hdp_dma_rx);
    
  /*##-4- Configure the NVIC for DMA #########################################*/
  /* NVIC configuration for DMA transfer complete interrupt (USART_CL_TX) */
  HAL_NVIC_SetPriority(USART_DP_DMA_TX_IRQn, 2, 1);
  HAL_NVIC_EnableIRQ(USART_DP_DMA_TX_IRQn);
    
  /* NVIC configuration for DMA transfer complete interrupt (USART_CL_RX) */
  HAL_NVIC_SetPriority(USART_DP_DMA_RX_IRQn, 4, 0);   
  HAL_NVIC_EnableIRQ(USART_DP_DMA_RX_IRQn);
  
  /* NVIC configuration for USART TC interrupt */
  HAL_NVIC_SetPriority(USART_DP_IRQn, 4, 0);
  HAL_NVIC_EnableIRQ(USART_DP_IRQn);
}

/**
  * @brief UART MSP Initialization 
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  *           - DMA configuration for transmission request by peripheral 
  *           - NVIC configuration for DMA interrupt request enable
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspInit(UART_HandleTypeDef *huart)
{
  /* do not need to check the input arguments, since the HAL driver has done that already */
	
	if (huart->Instance == USART_LL)
	{
		/* UART for the low level hardware */
		initProjectUSARTForLowLevelHardware(huart);
	}
	else if (huart->Instance == USART_CL)
	{
		initProjectUSARTForCommandLine(huart);
	}
	else if (huart->Instance == USART_DP)
	{
		initProjectUSARTForDebugPrint(huart);
	}
	else
	{
		/* this should generate an error */
	}
}

/**
  * @brief UART MSP De-Initialization for the low level hardware
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO, DMA and NVIC configuration to their default state
  * @param huart: UART handle pointer
  * @retval None
  */
void deinitProjectUSARTForLowLevelHardware(UART_HandleTypeDef *huart)
{
	static DMA_HandleTypeDef hdma_tx;
  static DMA_HandleTypeDef hdma_rx;

  /*##-1- Reset peripherals ##################################################*/
  USART_LL_FORCE_RESET();
  USART_LL_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks #################################*/
  /* Configure UART Tx as alternate function  */
  HAL_GPIO_DeInit(USART_LL_TX_GPIO_PORT, USART_LL_TX_PIN);
  /* Configure UART Rx as alternate function  */
  HAL_GPIO_DeInit(USART_LL_RX_GPIO_PORT, USART_LL_RX_PIN);
   
  /*##-3- Disable the DMA Streams ############################################*/
  /* De-Initialize the DMA Stream associate to transmission process */
  HAL_DMA_DeInit(&hdma_tx); 
  /* De-Initialize the DMA Stream associate to reception process */
  HAL_DMA_DeInit(&hdma_rx);
  
  /*##-4- Disable the NVIC for DMA ###########################################*/
  HAL_NVIC_DisableIRQ(USART_LL_DMA_TX_IRQn);
  HAL_NVIC_DisableIRQ(USART_LL_DMA_RX_IRQn);
}

/**
  * @brief UART MSP De-Initialization for the command line
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO, DMA and NVIC configuration to their default state
  * @param huart: UART handle pointer
  * @retval None
  */
void deinitProjectUSARTForCommandLine(UART_HandleTypeDef *huart)
{
	static DMA_HandleTypeDef hcl_dma_tx;
  static DMA_HandleTypeDef hcl_dma_rx;

  /*##-1- Reset peripherals ##################################################*/
  USART_CL_FORCE_RESET();
  USART_CL_RELEASE_RESET();

  /*##-2- Disable peripherals and GPIO Clocks #################################*/
  /* Configure UART Tx as alternate function  */
  HAL_GPIO_DeInit(USART_CL_TX_GPIO_PORT, USART_CL_TX_PIN);
  /* Configure UART Rx as alternate function  */
  HAL_GPIO_DeInit(USART_CL_RX_GPIO_PORT, USART_CL_RX_PIN);
   
  /*##-3- Disable the DMA Streams ############################################*/
  /* De-Initialize the DMA Stream associate to transmission process */
  HAL_DMA_DeInit(&hcl_dma_tx); 
  /* De-Initialize the DMA Stream associate to reception process */
  HAL_DMA_DeInit(&hcl_dma_rx);
  
  /*##-4- Disable the NVIC for DMA ###########################################*/
  HAL_NVIC_DisableIRQ(USART_CL_DMA_TX_IRQn);
  HAL_NVIC_DisableIRQ(USART_CL_DMA_RX_IRQn);
}


/**
  * @brief UART MSP De-Initialization 
  *        This function frees the hardware resources used in this example:
  *          - Disable the Peripheral's clock
  *          - Revert GPIO, DMA and NVIC configuration to their default state
  * @param huart: UART handle pointer
  * @retval None
  */
void HAL_UART_MspDeInit(UART_HandleTypeDef *huart)
{
  /* do not need to check the input arguments, since the HAL driver has done that already */
	
	if (huart == &llUartHandle)
	{
		/* UART for the low level hardware */
		deinitProjectUSARTForLowLevelHardware(huart);
	}
	else if (huart == &clUartHandle)
	{
		deinitProjectUSARTForCommandLine(huart);
	}
	else
	{
		/* this should generate an error */
	}
  
}

/**
  * @brief TIM MSP Initialization for HEAD1 motor
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  * @param htim: TIM handle pointer
  * @retval None
  */
void initTIM_SSONIC(TIM_HandleTypeDef *htim)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIM_SSONIC_CLK_ENABLE();
	
	/*##-2- Configure the GPIO          ########################################*/
	
	SSONIC_GPIO_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = SSONIC_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  //GPIO_InitStruct.Alternate = SSONIC_GPIO_AF;
  
  HAL_GPIO_Init(SSONIC_GPIO_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin(SSONIC_GPIO_PORT, SSONIC_PIN, GPIO_PIN_RESET);
	
	/* SSONIC_2 */
	SSONIC_2_GPIO_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = SSONIC_2_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(SSONIC_2_GPIO_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin(SSONIC_2_GPIO_PORT, SSONIC_2_PIN, GPIO_PIN_RESET);
	
	/* SSONIC_3 */
	SSONIC_3_GPIO_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = SSONIC_3_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_OUTPUT_PP;;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(SSONIC_3_GPIO_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin(SSONIC_3_GPIO_PORT, SSONIC_3_PIN, GPIO_PIN_RESET);
	
  /*##-2- Configure the NVIC for TIMx ########################################*/
  /* Set Interrupt Group Priority */ 
  HAL_NVIC_SetPriority(TIM_SSONIC_IRQn, 4, 0);
  
  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM_SSONIC_IRQn);
}

/**
  * @brief TIM MSP Initialization for SSONIC_DETECT
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  * @param htim: TIM handle pointer
  * @retval None
  */
void initTIM_SSONIC_DETECT(TIM_HandleTypeDef *htim)
{	
	GPIO_InitTypeDef  GPIO_InitStruct;
	
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIM_SSONIC_DETECT_CLK_ENABLE();
	
	TIM_SSONIC_DETECT_GPIO_CLK_ENABLE();
	
	/*##-2- GPIO */
	
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Pin  = TIM_SSONIC_DETECT_PIN;
  HAL_GPIO_Init(TIM_SSONIC_DETECT_GPIO_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin(TIM_SSONIC_DETECT_GPIO_PORT, TIM_SSONIC_DETECT_PIN, GPIO_PIN_RESET);
	
	/* SSONIC_2 */
	TIM_SSONIC_2_DETECT_GPIO_CLK_ENABLE();
	
	/*##-2- GPIO */
	
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Pin  = TIM_SSONIC_2_DETECT_PIN;
  HAL_GPIO_Init(TIM_SSONIC_2_DETECT_GPIO_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin(TIM_SSONIC_2_DETECT_GPIO_PORT, TIM_SSONIC_2_DETECT_PIN, GPIO_PIN_RESET);
	
	/* SSONIC_3 */
	TIM_SSONIC_3_DETECT_GPIO_CLK_ENABLE();
	
	/*##-2- GPIO */
	
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Pin  = TIM_SSONIC_3_DETECT_PIN;
  HAL_GPIO_Init(TIM_SSONIC_3_DETECT_GPIO_PORT, &GPIO_InitStruct);
	HAL_GPIO_WritePin(TIM_SSONIC_3_DETECT_GPIO_PORT, TIM_SSONIC_3_DETECT_PIN, GPIO_PIN_RESET);
	
	/*##-3- Configure the NVIC for TIM_SSONIC_DETECT_GPIO */
	HAL_NVIC_SetPriority(TIM_SSONIC_DETECT_GPIO_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(TIM_SSONIC_DETECT_GPIO_IRQn);
	
	HAL_NVIC_SetPriority(TIM_SSONIC_2_DETECT_GPIO_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(TIM_SSONIC_2_DETECT_GPIO_IRQn);
	
	HAL_NVIC_SetPriority(TIM_SSONIC_3_DETECT_GPIO_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(TIM_SSONIC_3_DETECT_GPIO_IRQn);
	
  /*##-4- Configure the NVIC for TIM_SSONIC_DETECT ###########################*/
  /* Set Interrupt Group Priority */ 
  HAL_NVIC_SetPriority(TIM_SSONIC_DETECT_IRQn, 4, 0);
  
  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM_SSONIC_DETECT_IRQn);
}

/**
  * @brief TIM MSP Initialization for SSONIC_LOOP timer
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  * @param htim: TIM handle pointer
  * @retval None
  */
void initTIM_SSONIC_LOOP(TIM_HandleTypeDef *htim)
{
	/*##-1- Enable peripherals Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIM_SSONIC_LOOP_CLK_ENABLE();
	
	/*##-2- Configure the NVIC for TIMx ########################################*/
  /* Set Interrupt Group Priority */ 
  HAL_NVIC_SetPriority(TIM_SSONIC_LOOP_IRQn, 4, 0);
  
  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM_SSONIC_LOOP_IRQn);
}

/**
  * @brief TIM MSP Initialization for HEAD1 motor
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  * @param htim: TIM handle pointer
  * @retval None
  */
void HAL_TIM_Base_MspInit(TIM_HandleTypeDef *htim)
{
  if (TIM_SSONIC == htim->Instance)
	{
		initTIM_SSONIC(htim);
	}
	else if (TIM_SSONIC_DETECT == htim->Instance)
	{
		initTIM_SSONIC_DETECT(htim);
	}
	else if (TIM_SSONIC_LOOP == htim->Instance)
	{
		initTIM_SSONIC_LOOP(htim);
	}
	else
	{
		/* error */
	}
}

/**
  * @brief TIM MSP Initialization for HEAD1_PWM
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  * @param htim: TIM handle pointer
  * @retval None
  */
void initTIMHEAD1PWM(TIM_HandleTypeDef *htim)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIM_HEAD1_PWM_CLK_ENABLE();
	
	/*##-2- Configure the GPIO          ########################################*/
	
	HEAD1_PUL_PWM_GPIO_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = HEAD1_PUL_PWM_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = HEAD1_PUL_PWM_GPIO_AF;
  
  HAL_GPIO_Init(HEAD1_PUL_PWM_GPIO_PORT, &GPIO_InitStruct);
  
  /*##-3- Configure the NVIC for TIMx ########################################*/
  /* Set Interrupt Group Priority */ 
  HAL_NVIC_SetPriority(TIM_HEAD1_PWM_IRQn, 4, 0);
  
  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM_HEAD1_PWM_IRQn);
}

/**
  * @brief TIM MSP Initialization for HEAD2_PWM
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  * @param htim: TIM handle pointer
  * @retval None
  */
void initTIMHEAD2PWM(TIM_HandleTypeDef *htim)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
  /*##-1- Enable peripherals and GPIO Clocks #################################*/
  /* TIMx Peripheral clock enable */
  TIM_HEAD2_PWM_CLK_ENABLE();
	
	/*##-2- Configure the GPIO          ########################################*/
	
	HEAD2_PUL_PWM_GPIO_CLK_ENABLE();
	
	GPIO_InitStruct.Pin       = HEAD2_PUL_PWM_PIN;
  GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull      = GPIO_NOPULL;
  GPIO_InitStruct.Speed     = GPIO_SPEED_FAST;
  GPIO_InitStruct.Alternate = HEAD2_PUL_PWM_GPIO_AF;
  
  HAL_GPIO_Init(HEAD2_PUL_PWM_GPIO_PORT, &GPIO_InitStruct);
  
  /*##-3- Configure the NVIC for TIMx ########################################*/
  /* Set Interrupt Group Priority */ 
  HAL_NVIC_SetPriority(TIM_HEAD2_PWM_IRQn, 4, 0);
  
  /* Enable the TIMx global Interrupt */
  HAL_NVIC_EnableIRQ(TIM_HEAD2_PWM_IRQn);
}

/**
  * @brief TIM MSP Initialization
  *        This function configures the hardware resources used in this example: 
  *           - Peripheral's clock enable
  *           - Peripheral's GPIO Configuration  
  * @param htim: TIM handle pointer
  * @retval None
  */
void HAL_TIM_PWM_MspInit (TIM_HandleTypeDef * htim)
{
	if (TIM_HEAD1_PWM == htim->Instance)
	{
		initTIMHEAD1PWM(htim);
	}
	else if (TIM_HEAD2_PWM == htim->Instance)
	{
		initTIMHEAD2PWM(htim);
	}
	else
	{
		/* error */
	}
}

/**
  * @brief  Configures GPIO for the HEAD1_ENABLE.
  * @param  None.
  * @retval None.
  */
void PROJECT_HEAD1_ENABLE_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the HEAD1_ENABLE Clock */
  HEAD1_ENABLE_GPIO_CLK_ENABLE();

  /* Configure the HEAD1_ENABLE pin */
  GPIO_InitStruct.Pin   = HEAD1_ENABLE_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(HEAD1_ENABLE_GPIO_PORT, &GPIO_InitStruct);
  
  HAL_GPIO_WritePin(HEAD1_ENABLE_GPIO_PORT, HEAD1_ENABLE_PIN, GPIO_PIN_SET); 
}

/**
  * @brief  Configures GPIO for the HEAD1_DIR.
  * @param  None.
  * @retval None.
  */
void PROJECT_HEAD1_DIR_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the HEAD1_ENABLE Clock */
  HEAD1_DIR_GPIO_CLK_ENABLE();

  /* Configure the HEAD1_ENABLE pin */
  GPIO_InitStruct.Pin   = HEAD1_DIR_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(HEAD1_DIR_GPIO_PORT, &GPIO_InitStruct);
  
  HAL_GPIO_WritePin(HEAD1_DIR_GPIO_PORT, HEAD1_DIR_PIN, GPIO_PIN_SET); 
}

/**
  * @brief  Configures GPIO for the HEAD1_OPTO.
  * @param  None.
  * @retval None.
  */
void PROJECT_HEAD1_OPTO_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the HEAD1_ENABLE Clock */
  HEAD1_OPTO_GPIO_CLK_ENABLE();

  /* Configure the HEAD1_ENABLE pin */
  GPIO_InitStruct.Pin   = HEAD1_OPTO_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(HEAD1_OPTO_GPIO_PORT, &GPIO_InitStruct);
  
  HAL_GPIO_WritePin(HEAD1_OPTO_GPIO_PORT, HEAD1_OPTO_PIN, GPIO_PIN_SET); 
}

/**
  * @brief  Configures GPIO for the HEAD2_ENABLE.
  * @param  None.
  * @retval None.
  */
void PROJECT_HEAD2_ENABLE_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the HEAD1_ENABLE Clock */
  HEAD2_ENABLE_GPIO_CLK_ENABLE();

  /* Configure the HEAD1_ENABLE pin */
  GPIO_InitStruct.Pin   = HEAD2_ENABLE_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(HEAD2_ENABLE_GPIO_PORT, &GPIO_InitStruct);
  
  HAL_GPIO_WritePin(HEAD2_ENABLE_GPIO_PORT, HEAD2_ENABLE_PIN, GPIO_PIN_SET); 
}

/**
  * @brief  Configures GPIO for the HEAD2_DIR.
  * @param  None.
  * @retval None.
  */
void PROJECT_HEAD2_DIR_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the HEAD1_ENABLE Clock */
  HEAD2_DIR_GPIO_CLK_ENABLE();

  /* Configure the HEAD1_ENABLE pin */
  GPIO_InitStruct.Pin   = HEAD2_DIR_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(HEAD2_DIR_GPIO_PORT, &GPIO_InitStruct);
  
  HAL_GPIO_WritePin(HEAD2_DIR_GPIO_PORT, HEAD2_DIR_PIN, GPIO_PIN_SET); 
}

/**
  * @brief  Configures GPIO for the HEAD2_OPTO.
  * @param  None.
  * @retval None.
  */
void PROJECT_HEAD2_OPTO_Init(void)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the HEAD1_ENABLE Clock */
  HEAD2_OPTO_GPIO_CLK_ENABLE();

  /* Configure the HEAD1_ENABLE pin */
  GPIO_InitStruct.Pin   = HEAD2_OPTO_PIN;
  GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull  = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
  
  HAL_GPIO_Init(HEAD2_OPTO_GPIO_PORT, &GPIO_InitStruct);
  
  HAL_GPIO_WritePin(HEAD2_OPTO_GPIO_PORT, HEAD2_OPTO_PIN, GPIO_PIN_SET); 
}

/**
  * @}
  */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
