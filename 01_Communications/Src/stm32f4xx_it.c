/**
  ******************************************************************************
  * @file    stm32f4xx_it.c 
  * @author  MCD Application Team + HU Yaoyu <huyaoyu@sjtu.edu.cn>
  * @version V1.2.3
  * @date    09-October-2015
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"

extern uint8_t currentEXTI;
   
/** @addtogroup STM32F4xx_HAL_Examples
  * @{
  */

/** @addtogroup UART_TwoBoards_ComDMA
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* UART handler declared in "main.c" file */
extern UART_HandleTypeDef llUartHandle;
extern UART_HandleTypeDef clUartHandle;
extern UART_HandleTypeDef dpUartHandle;

extern TIM_HandleTypeDef TimHandle_SSONIC;
extern TIM_HandleTypeDef TimHandle_SSONIC_DETECT;
extern TIM_HandleTypeDef TimHandle_SSONIC_LOOP;
extern TIM_HandleTypeDef TimHandle_HEAD1PWM;
extern TIM_HandleTypeDef TimHandle_HEAD2PWM;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief  This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
void SysTick_Handler(void)
{
  HAL_IncTick();
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
  * @brief  This function handles DMA RX interrupt request.  
  * @param  None
  * @retval None   
  */
void USART_LL_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(llUartHandle.hdmarx);
}

/**
  * @brief  This function handles DMA TX interrupt request.
  * @param  None
  * @retval None   
  */
void USART_LL_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(llUartHandle.hdmatx);
}

/**
  * @brief  This function handles USART_LL interrupt request.
  * @param  None
  * @retval None
  */
void USART_LL_IRQHandler(void)
{
  HAL_UART_IRQHandler(&llUartHandle);
}

/**
  * @brief  This function handles DMA RX interrupt request of the command line.  
  * @param  None
  * @retval None   
  */
void USART_CL_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(clUartHandle.hdmarx);
}

/**
  * @brief  This function handles DMA TX interrupt request of the command line.
  * @param  None
  * @retval None   
  */
void USART_CL_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(clUartHandle.hdmatx);
}

/**
  * @brief  This function handles USART_LL interrupt request of the command line.
  * @param  None
  * @retval None
  */
void USART_CL_IRQHandler(void)
{
  HAL_UART_IRQHandler(&clUartHandle);
}


/**
  * @brief  This function handles DMA RX interrupt request of the debug-print.  
  * @param  None
  * @retval None   
  */
void USART_DP_DMA_RX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(dpUartHandle.hdmarx);
}

/**
  * @brief  This function handles DMA TX interrupt request of the debug-print.
  * @param  None
  * @retval None   
  */
void USART_DP_DMA_TX_IRQHandler(void)
{
  HAL_DMA_IRQHandler(dpUartHandle.hdmatx);
}

/**
  * @brief  This function handles USART_DP interrupt request of the debug-print.
  * @param  None
  * @retval None
  */
void USART_DP_IRQHandler(void)
{
  HAL_UART_IRQHandler(&dpUartHandle);
}

/**
  * @brief  This function handles SSONIC timer interrupt request.
  * @param  None
  * @retval None
  */
void TIM_SSONIC_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&TimHandle_SSONIC);
}

/**
  * @brief  This function handles SSONIC_DETECT interrupt request.
  * @param  None
  * @retval None
  */
void TIM_SSONIC_DETECT_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&TimHandle_SSONIC_DETECT);
}

/**
  * @brief  This function handles SSONIC_LOOP interrupt request.
  * @param  None
  * @retval None
  */
void TIM_SSONIC_LOOP_IRQHandler(void)
{
	HAL_TIM_IRQHandler(&TimHandle_SSONIC_LOOP);
}

/**
  * @brief  This function handles SSONIC_DETECT_GPIO EXTI interrupt request.
  * @param  None
  * @retval None
  */
void TIM_SSONIC_DETECT_GPIO_IRQHandler(void)
{
	currentEXTI = EXTI_SSONIC_DETECT_GPIO_TRIGERED;
	HAL_GPIO_EXTI_IRQHandler(TIM_SSONIC_DETECT_PIN);
}

/**
  * @brief  This function handles SSONIC_2_DETECT_GPIO EXTI interrupt request.
  * @param  None
  * @retval None
  */
void TIM_SSONIC_2_DETECT_GPIO_IRQHandler(void)
{
	currentEXTI = EXTI_SSONIC_2_DETECT_GPIO_TRIGERED;
	HAL_GPIO_EXTI_IRQHandler(TIM_SSONIC_2_DETECT_PIN);
}

/**
  * @brief  This function handles SSONIC_2_DETECT_GPIO EXTI interrupt request.
  * @param  None
  * @retval None
  */
void TIM_SSONIC_3_DETECT_GPIO_IRQHandler(void)
{
	currentEXTI = EXTI_SSONIC_3_DETECT_GPIO_TRIGERED;
	HAL_GPIO_EXTI_IRQHandler(TIM_SSONIC_3_DETECT_PIN);
}

/**
  * @brief  This function handles HEAD1_PWM interrupt request.
  * @param  None
  * @retval None
  */
void TIM_HEAD1_PWM_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&TimHandle_HEAD1PWM);
}

/**
  * @brief  This function handles HEAD2_PWM interrupt request.
  * @param  None
  * @retval None
  */
void TIM_HEAD2_PWM_IRQHandler(void)
{
  HAL_TIM_IRQHandler(&TimHandle_HEAD2PWM);
}

/**
  * @brief  This function handles MAG_1 interrupt request.
  * @param  None
  * @retval None
  */
void MAG_1_GPIO_IRQHandler(void)
{
	currentEXTI = MAG_1_GPIO_TRIGERED;
	HAL_GPIO_EXTI_IRQHandler(MAG_1_PIN);
}

/**
  * @brief  This function handles MAG_2 interrupt request.
  * @param  None
  * @retval None
  */
void MAG_2_GPIO_IRQHandler(void)
{
	currentEXTI = MAG_2_GPIO_TRIGERED;
	HAL_GPIO_EXTI_IRQHandler(MAG_2_PIN);
}

/**
  * @brief  This function handles MAG_3 interrupt request.
  * @param  None
  * @retval None
  */
void MAG_3_GPIO_IRQHandler(void)
{
	currentEXTI = MAG_3_GPIO_TRIGERED;
	HAL_GPIO_EXTI_IRQHandler(MAG_3_PIN);
}

/**
  * @brief  This function handles MAG_3 interrupt request.
  * @param  None
  * @retval None
  */
void MAG_4_GPIO_IRQHandler(void)
{
	currentEXTI = MAG_4_GPIO_TRIGERED;
	HAL_GPIO_EXTI_IRQHandler(MAG_4_PIN);
}


/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
