/**
  ******************************************************************************
  * @file    main.c 
  * @author  MCD Application Team
  * @version V1.2.3
  * @date    09-October-2015
  * @brief   This is the main.c source file
  ******************************************************************************
  * @attention
  *
  * 
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "CommonHeader.h"
#include "ProjectUtils.h"
#include "Modbus.h"
#include "ProjectControl.h"

#include <stdio.h> /* strng operations */
#include <string.h>
#include <stdlib.h>


/** @addtogroup STM32F4xx_HAL_Examples
  * @{
  */

/** @addtogroup UART_TwoBoards_ComDMA
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
//#define PROJECT_TEST_RUN

/* Private macro -------------------------------------------------------------*/

#define CL_BUFFER_SIZE  64 /* the size of the command line buffer */
#define DP_BUFFER_SIZE 1024 /* the size of the debug-print buffer */
#define DP_MAX_LINES   9999 /* the maximum lines of debug-print */
/* NOTE: Onec the number of debug-print lines is higher than DP_MAX_LINES,
 *       the index will re-start from 0. */
#define DP_INDEX_WIDTH   6 /* the width of the index string in the debug-print line */

#define WRITE_DEBUG_PRINT(x) \
	writeDebugPrint(x, &debugPrintCounter); \
	debugPrintCounter++;

#define COMMAND_ADDRESS (0x01)          /* the address of this mechine */
#define COMMAND_LENGTH  (12)            /* the command length, in byte */
#define COMMAND_LINE_CHECK_INTERVAL (5) /* unit: ms */

#define DEBUG_STRING_SIZE 512

#define REC_BUFFER_TEST_VEL_LL 32

#define PROJECT_HEAD1_PWM_MAX_ANGLE      (PROJECT_PI)      /* rad */
#define PROJECT_HEAD1_PWM_MIN_ANGLE      (-PROJECT_PI)     /* rad */
#define PROJECT_HEAD1_PWM_ANGLE_PER_STEP (0.00117)         /* rad */
//#define PROJECT_HEAD1_PWM_ANGLE_PER_STEP   (0.00165)     /* rad */

#define PROJECT_HEAD2_PWM_MAX_ANGLE      (1.5*( PROJECT_PI))  /* rad */
#define PROJECT_HEAD2_PWM_MIN_ANGLE      (1.5*(-PROJECT_PI))  /* rad */
#define PROJECT_HEAD2_PWM_ANGLE_PER_STEP (0.00117)            /* rad */

#define SEND_INFO_POS_WHEEL   (2)
#define SEND_INFO_POS_HEAD    (6)
#define SEND_INFO_POS_SSONIC (10)

/* Private variables ---------------------------------------------------------*/

char debugString[DEBUG_STRING_SIZE];

uint8_t recBuffer_TestVelocityFromLowLevelHardware[REC_BUFFER_TEST_VEL_LL];
ProjectBuffer_t projRecBuffer_TestVelLL;


/* =============== EXTIs ================= */

uint8_t currentEXTI = EXTI_NONE_TRIGERED;

/* ============== USARTs ================= */

uint32_t debugPrintCounter = 0;         /* index of debug-print string */
char dpTempString[DP_BUFFER_SIZE];      /* temporary string of debug-print */
uint8_t dpTempTxBuffer[DP_BUFFER_SIZE]; /* temporary Tx Buffer of debug-print */

/* UART handler declaration */
UART_HandleTypeDef llUartHandle;   /* USART for low-level hardware */
UART_HandleTypeDef clUartHandle;   /* USART for command line */
UART_HandleTypeDef dpUartHandle;   /* USART for debug-print */
__IO ITStatus llUartReady     = RESET; /* the status flag for the low-level USART */
__IO ITStatus llUartReady_rec = RESET;
__IO ITStatus clUartReady     = RESET; /* the status flag for the command line */
__IO ITStatus dpUartReady     = RESET; /* the status flag for the debug-print */
__IO uint32_t clByteCounter   = 0;     /* the counter of the accumulated data for the command line */
__IO ITStatus clCommandReady  = RESET; /* the status flag for the command line */


/* Buffer used for transmission */
uint8_t aTxBuffer[64];
uint8_t clTxBuffer[CL_BUFFER_SIZE];
uint8_t dpTxBuffer[DP_BUFFER_SIZE];
ProjectBuffer_t msgTxBuffer;
ProjectBuffer_t clTxProjBuffer;
ProjectBuffer_t dpTxProjBuffer;

/* Buffer used for reception */
uint8_t aRxBuffer[RXBUFFERSIZE];
uint8_t clRxBuffer[CL_BUFFER_SIZE]; /* receiver buffer for the command line */
uint8_t clCommandBuffer[CL_BUFFER_SIZE]; /* the comand buffer */
uint8_t dpRxBuffer[DP_BUFFER_SIZE]; /* receiver buffer for the debug-print */
ProjectBuffer_t msgRxBuffer;
ProjectBuffer_t clRxProjBuffer;
ProjectBuffer_t clCommandProjBuffer;
ProjectBuffer_t dpRxProjBuffer;

/* ============== Timer and PWM ================= */

/* Timer for SSONIC */
TIM_HandleTypeDef TimHandle_SSONIC;
uint32_t uwPrescaler_SSONIC = 0;
__IO uint32_t uwSSONICCounter  = 0;
__IO uint8_t Project_SSONIC_InProgress = PROJECT_NOTHING_IN_PROGRESS;

TIM_HandleTypeDef TimHandle_SSONIC_DETECT;
uint32_t uwPrescaler_SSONIC_DETECT      = 0;
__IO uint32_t uwSSONIC_DETECT_Counter   = 0;
__IO  uint8_t timerStatus_SSONIC_DETECT = TIM_SSONIC_DETECT_HALT;
__IO uint32_t uwSSONIC_DETECT_Distance  = 0; /* in mm */
__IO ITStatus SSONIC_DETECT_READY       = RESET;
__IO ITStatus SSONIC_DETECT_GPIO_Rising = RESET;

__IO ProjectSSONICRepresentation_t* pCurrentPSSR  = NULL;
__IO ProjectSSONICRepresentation_t* pPreviousPSSR = NULL;
ProjectSSONICRepresentation_t SSONIC_1;
ProjectSSONICRepresentation_t SSONIC_2;
ProjectSSONICRepresentation_t SSONIC_3;
__IO ITStatus SSONIC_LOOP_READY = RESET;

/* SSONIC_LOOP timer */

TIM_HandleTypeDef TimHandle_SSONIC_LOOP;
uint32_t uwPrescaler_SSONIC_LOOP = 0;

/* Timer for HEAD1_PWM */
TIM_HandleTypeDef TimHandle_HEAD1PWM;
uint32_t uwPrescaler_HEAD1PWM = 0;
TIM_OC_InitTypeDef TimHEAD1PWM_OCInit;
ProjectStepMotorRepresentation_t smrHEAD1PWM;

/* Timer for HEAD2_PWM */
TIM_HandleTypeDef TimHandle_HEAD2PWM;
uint32_t uwPrescaler_HEAD2PWM = 0;
TIM_OC_InitTypeDef TimHEAD2PWM_OCInit;
ProjectStepMotorRepresentation_t smrHEAD2PWM;

uint32_t HEAD2PWM_ReverseCounterArray[1024];
uint32_t posReverseCounterArray = 0;

/* ============== Magnetic sensors ================= */
#define PROJECT_NUM_MEG_SENSORS 4
ProjectMagSRepresentation_t magsArray[PROJECT_NUM_MEG_SENSORS];



/* debug the command line */

uint32_t recCommandCounter = 0;

/* Private function prototypes -----------------------------------------------*/

static void SystemClock_Config(void);
static void Error_Handler(void);
static void waitOnCommandLine(void);
static ProjectStatus_t sendInfoToUpperMechine(void);
static void querySSONICs(void);
static void writeDebugPrint(const char* projBuffer, uint32_t* counter);

static void initUSART_LL(void);
static void initUSART_CL(void);
static void initUSART_DP(void);
static void initHEAD1(void);
static void initHEAD2(void);
static void initSSONIC(void);
static void initSSONIC_DETECT(void);
static void initSSONIC_LOOP(void);
static void initMagSensors(ProjectMagSRepresentation_t* megsArrayHead, uint8_t n);

static ProjectStatus_t showHeadMotorLimiters(void);

static ProjectStatus_t testGenerageError(uint32_t level);
static ProjectStatus_t testHeadMotors(void);
static ProjectStatus_t testHeadMotorSearchForZeroPoint(ProjectStepMotorRepresentation_t *pPSMR);
static ProjectStatus_t testHeadMotorSearchForZeroPointWithCommandLine(void);
static ProjectStatus_t testReadVelocityFromLowLevelHardware(void);

/* Private functions ---------------------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
  /* STM32F4xx HAL library initialization:
       - Configure the Flash prefetch, instruction and Data caches
       - Configure the Systick to generate an interrupt each 1 msec
       - Set NVIC Group Priority to 4
       - Global MSP (MCU Support Package) initialization
     */
  HAL_Init();
  
  /* Configure LED3, LED4, LED5 and LED6 */
  BSP_LED_Init(LED3);
  BSP_LED_Init(LED4);
  BSP_LED_Init(LED5);
  BSP_LED_Init(LED6);

  /* Configure the system clock to 168 MHz */
  SystemClock_Config();
	
  /*##-1- Configure the UART peripheral ######################################*/
  initUSART_LL(); /* initialize low-level hardware USART */
	initUSART_CL(); /* initialize command line USART */
	initUSART_DP(); /* initialize debug-print USART */
  
	/* initialization of the global variables */
	msgTxBuffer.buffer = aTxBuffer;
	msgTxBuffer.len    = 64;
	msgRxBuffer.buffer = aRxBuffer;
	msgRxBuffer.len    = 64;
	
	clTxProjBuffer.buffer = clTxBuffer;
	clTxProjBuffer.len    = CL_BUFFER_SIZE;
	clRxProjBuffer.buffer = clRxBuffer;
	clRxProjBuffer.len    = CL_BUFFER_SIZE;
	clCommandProjBuffer.buffer = clCommandBuffer;
	clCommandProjBuffer.len    = CL_BUFFER_SIZE;
	
	dpTxProjBuffer.buffer = dpTxBuffer;
	dpTxProjBuffer.len    = DP_BUFFER_SIZE;
	dpRxProjBuffer.buffer = dpRxBuffer;
	dpRxProjBuffer.len    = DP_BUFFER_SIZE;
	
	projRecBuffer_TestVelLL.buffer = recBuffer_TestVelocityFromLowLevelHardware;
	projRecBuffer_TestVelLL.len    = REC_BUFFER_TEST_VEL_LL;
	
	/* ============= initialize other hardware ============= */
	/* Configure USER Button */
  BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
	/* HEAD1_PWM step motor */
	initHEAD1();
	/* HEAD1_PWM step motor */
	initHEAD2();
	/* super sonic sensors */
	initSSONIC();
	/* deliberate delay for the super sonic sensor */
	HAL_Delay(500);
	/* detector for the super sonic sensors */
	initSSONIC_DETECT();
	/* loop timer for the super sonic sensors */
	initSSONIC_LOOP();
	/* deliberate delay for the super sonic sensor */
	HAL_Delay(500);
	/* magnetic sensors */
	
	magsArray[0].GPIOPin       = MAG_1_PIN;
	magsArray[0].GPIOPort      = MAG_1_GPIO_PORT;
	magsArray[0].GPIOEXTI_IRQn = MAG_1_GPIO_IRQn;
	magsArray[0].reached       = PROJECT_MAG_SENSOR_NOT_REACHED;
	magsArray[0].id            = 0;
	magsArray[1].GPIOPin       = MAG_2_PIN;
	magsArray[1].GPIOPort      = MAG_2_GPIO_PORT;
	magsArray[1].GPIOEXTI_IRQn = MAG_2_GPIO_IRQn;
	magsArray[1].reached       = PROJECT_MAG_SENSOR_NOT_REACHED;
	magsArray[1].id            = 1;
	magsArray[2].GPIOPin       = MAG_3_PIN;
	magsArray[2].GPIOPort      = MAG_3_GPIO_PORT;
	magsArray[2].GPIOEXTI_IRQn = MAG_3_GPIO_IRQn;
	magsArray[2].reached       = PROJECT_MAG_SENSOR_NOT_REACHED;
	magsArray[2].id            = 2;
	magsArray[3].GPIOPin       = MAG_4_PIN;
	magsArray[3].GPIOPort      = MAG_4_GPIO_PORT;
	magsArray[3].GPIOEXTI_IRQn = MAG_4_GPIO_IRQn;
	magsArray[3].reached       = PROJECT_MAG_SENSOR_NOT_REACHED;
	magsArray[3].id            = 3;
	initMagSensors(magsArray, PROJECT_NUM_MEG_SENSORS);
  
	/* initialize the project control */
	if (PROJECT_OK != initProjectControl())
	{
		/* error */
		Error_Handler();
	}
	
	/* print information to the debug-print at the end of initialization */
	WRITE_DEBUG_PRINT("Initialize...");
	
//	while(1)
//	{
//		sendCommandToLowLevelHardware(0x01, 0x06, 0x0043, 0x0064);
//		HAL_Delay(100);
//	}
	
  /* main control loop execution */
	pCurrentPSSR  = &SSONIC_1;
	pPreviousPSSR = pCurrentPSSR;
	if (PROJECT_OK != startSSONIC_LOOP())
	{
		/* error */
		Error_Handler();
	}
	
	recCommandCounter = 0;
	
#ifdef PROJECT_TEST_RUN
//	while(1)
//	{
//		testHeadMotors();
//	}
	
	/* test search for zero point */
//	testHeadMotorSearchForZeroPoint(&smrHEAD1PWM);
//	HAL_Delay(1000);
//	testHeadMotorSearchForZeroPoint(&smrHEAD2PWM);
	
	/* test search for zero point with command line */
//	testHeadMotorSearchForZeroPointWithCommandLine();
//	while (1)
//	{
//	}
	
	/* test read velocity from low level hardware */
	while (1)
	{
		testReadVelocityFromLowLevelHardware();
	}
	
#else
	
	clByteCounter  = 0;
	clCommandReady = RESET;
	
	while (1)
	{
		/* gather the data from every super sonic sensor */
//		if (SET == SSONIC_LOOP_READY)
//		{
//			if (PROJECT_OK != stopSSONIC_LOOP())
//			{
//				/* error */
//				Error_Handler();
//			}
//			else
//			{
//				SSONIC_LOOP_READY = RESET;
//				querySSONICs();
//				startSSONIC_LOOP();
//			}
//		}
		
		/* handle the command from the upper machine */
		waitOnCommandLine();
	}
#endif
}

/**
  * @brief  Handle the command from upper machine.
  * @param  None. 
  * @note   This function check the command from upper machine through the
  *         command line USART.
  * @retval None.
  */
static void waitOnCommandLine(void)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	uint8_t testByte    = 0;
	
	uint8_t transferProtocalCheck = 0;
	
	uint8_t addr      = 0x00;
	uint8_t func      = 0x00;
	uint16_t cmd[2]   = {0x0000,0x0000};
	uint16_t data[2]  = {0x0000,0x0000};
	
	uint8_t errorFlag = 0x00;
	
	ProjectDebugStringNode_t* pDebugStringNode = NULL;
	
	uint32_t clErrorCode          = 0;
	HAL_UART_StateTypeDef clState = HAL_UART_STATE_RESET;
	uint32_t clWaitCounter        = 0;
	
#ifdef TEST_ERROR_HANDLE
	/* test error handle */
	if (PROJECT_OK != testGenerageError(1))
	{
		if (PROJECT_OK != createDebugStringNode(&pDebugStringNode))
		{
			/* error */
			Error_Handler();
		}
		
		sprintf(debugString,"A error has been detected.");
		
		if ( PROJECT_OK != PROJECT_FILL_DEBUG_STRING_NODE(pDebugStringNode, debugString))
		{
			/* error */
			Error_Handler();
		}
		
		if (PROJECT_OK != appendDebugStringNode(&currentPDSN, pDebugStringNode))
		{
			/* error */
			Error_Handler();
		}
		
		/* error */
		errorFlag = 1;
		/* USE GOTO WITH GREAT CAUTION */
		goto WAIT_ON_COMMAND_ERROR;
	}
#endif
	
	/*##-1- try to receive command from the command line */
	clErrorCode = HAL_UART_GetError(&clUartHandle);
	if ( HAL_UART_ERROR_ORE == clErrorCode )
	{
		__HAL_UART_FLUSH_DRREGISTER(&clUartHandle);
		__HAL_UART_CLEAR_OREFLAG(&clUartHandle);
	}
	
	clState = HAL_UART_GetState(&clUartHandle);
	
	if ( HAL_UART_STATE_BUSY_RX    != clState && 
			 HAL_UART_STATE_BUSY_TX_RX != clState )
	{
		clCommandProjBuffer.contentLen = 0;
		
		if(HAL_UART_Receive_IT(&clUartHandle, (uint8_t *)(clRxProjBuffer.buffer), 1) != HAL_OK)
		{
			Error_Handler();
		}
	}
	
	while ( RESET == clCommandReady && clWaitCounter < 0x51615)
	{
		clWaitCounter++;
	}
	
	if ( 0x51615 != clWaitCounter )
	{
		
		clCommandReady = RESET;
//		recCommandCounter++;
		
		/*##-2- check the communication validity */
		
		if ( COMMAND_ADDRESS != clCommandProjBuffer.buffer[0] )
		{
			/* the address is wrong, just ignore this frame of data */
			return;
		}
		
		sta = checkTransferValidity(&clCommandProjBuffer, &transferProtocalCheck);
		
		if (PROJECT_OK != sta || transferProtocalCheck != 1)
		{
			/* error */
			errorFlag = 1;
			/* USE GOTO WITH GREAT CAUTION */
			goto WAIT_ON_COMMAND_ERROR;
		}
		
		sendInfoToUpperMechine();
		
		/*##-3- retrieve the command byte */
		
		BSP_LED_Toggle(LED3);
		recCommandCounter++;
			
		sta = retriveCommand(&clCommandProjBuffer, &addr, &func, cmd, data, 2);
		
		if (PROJECT_OK != sta)
		{
			/* error */
			errorFlag = 1;
			/* USE GOTO WITH GREAT CAUTION */
			goto WAIT_ON_COMMAND_ERROR;
		}
			
		/*##-4- command execution */
		
		/* execute */
		sta = executeCommand(func, cmd, data, 2);
		
		if (PROJECT_OK != sta)
		{
			/* error */
			errorFlag = 1;
			/* USE GOTO WITH GREAT CAUTION */
			goto WAIT_ON_COMMAND_ERROR;
		}
		
	//	showHeadMotorLimiters();
	}
	else
	{
		return;
	}
	
WAIT_ON_COMMAND_ERROR:
//	if (1 == errorFlag)
	if (0)
	{
		/* error handle */
		
		if (PROJECT_OK != composeDebugString(projectDSN, dpTempString, DP_BUFFER_SIZE) )
		{
			/* error */
			Error_Handler();
		}
		
		WRITE_DEBUG_PRINT(dpTempString);
		
		if (PROJECT_OK != releaseDebugStringStack(&(projectDSN->next)) )
		{
			/* error */
			Error_Handler();
		}
		
		projectDSN->next = NULL;
		currentPDSN = projectDSN;
	}
	
	return;
}

static ProjectStatus_t sendInfoToUpperMechine(void)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	int16_t headMotorAngle = 0x0000;
	int16_t ssonicDistance = 0x0000;
	
	uint16_t f1 = 0x0000;
	uint16_t f2 = 0x0000;
	uint16_t tempF = 0x0000;
	int16_t lv  = 0x0000;
	int16_t av  = 0x0000;
	
	ProjectFlag_t queryLowLevelHardwareFailedFlag = PROJECT_FLAG_NOT_SET;
	
	splitIntoArray(0x0100, clTxProjBuffer.buffer + 0 );
	
	/* gather information about the two wheels */
	if (PROJECT_OK != sendCommandToLowLevelHardware(0x01, 0x03, 0x0022, 0x0001, PROJECT_FLAG_SET, &projRecBuffer_TestVelLL, 7) )
	{
		WRITE_DEBUG_PRINT("M1 failed.")
		queryLowLevelHardwareFailedFlag = PROJECT_FLAG_SET;
	}
	HAL_Delay(1);
	
	f1 = projRecBuffer_TestVelLL.buffer[3];
	f1 = f1 << 8;
	tempF = projRecBuffer_TestVelLL.buffer[4];
	f1 = f1 | tempF;
	
	if (PROJECT_OK != sendCommandToLowLevelHardware(0x02, 0x03, 0x0022, 0x0001, PROJECT_FLAG_SET, &projRecBuffer_TestVelLL, 7) )
	{
		WRITE_DEBUG_PRINT("M2 failed.")
		queryLowLevelHardwareFailedFlag = PROJECT_FLAG_SET;
	}
	HAL_Delay(1);
	
	tempF = 0x0000;
	f2 = projRecBuffer_TestVelLL.buffer[3];
	f2 = f2 << 8;
	tempF = projRecBuffer_TestVelLL.buffer[4];
	f2 = f2 | tempF;
	
	if ( PROJECT_FLAG_SET == queryLowLevelHardwareFailedFlag )
	{
//		HAL_Delay(10);
		queryLowLevelHardwareFailedFlag = PROJECT_FLAG_NOT_SET;
		lv = 0x7FFF;
		av = 0x7FFF;
	}
	else
	{
		calculateLinearAngularVel(f1, f2, &lv, &av);
	}
	
	/* test use */
	splitIntoArray(lv, clTxProjBuffer.buffer + SEND_INFO_POS_WHEEL );
	splitIntoArray(av, clTxProjBuffer.buffer + SEND_INFO_POS_WHEEL + 2 );
	
	/* gather information about the head motors */
	
	if ( PROJECT_OK != getCurrentAngle_PSMR(&smrHEAD1PWM, &headMotorAngle) )
	{
		return sta;
	}
	
	splitIntoArray(headMotorAngle, clTxProjBuffer.buffer + SEND_INFO_POS_HEAD );
	
	if ( PROJECT_OK != getCurrentAngle_PSMR(&smrHEAD2PWM, &headMotorAngle) )
	{
		return sta;
	}
	
	splitIntoArray(headMotorAngle, clTxProjBuffer.buffer + SEND_INFO_POS_HEAD + 2 );
	
	/* gather information about the super sonic sensors */
	
	ssonicDistance = (uint16_t) (SSONIC_1.distance);
	splitIntoArray(ssonicDistance, clTxProjBuffer.buffer + SEND_INFO_POS_SSONIC );
	ssonicDistance = (uint16_t) (SSONIC_2.distance);
	splitIntoArray(ssonicDistance, clTxProjBuffer.buffer + SEND_INFO_POS_SSONIC + 2 );
	ssonicDistance = (uint16_t) (SSONIC_3.distance);
	splitIntoArray(ssonicDistance, clTxProjBuffer.buffer + SEND_INFO_POS_SSONIC + 4 );
	
	/* compose the message */
	
	clTxProjBuffer.contentLen = 16;
	addModbusSuffix(&clTxProjBuffer, clTxProjBuffer.contentLen);
	clTxProjBuffer.contentLen = 18;
	
	/* send the message */
	
	if(HAL_OK != HAL_UART_Transmit_DMA(&clUartHandle, (uint8_t*)(clTxProjBuffer.buffer), clTxProjBuffer.contentLen))
  {
    return sta;
  }
  
  /* Wait for the end of the transfer */  
  while (clUartReady != SET)
  {
  }
	
	clUartReady = RESET;
	
	sta = PROJECT_OK;
	
	return sta;
}

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 168000000
  *            HCLK(Hz)                       = 168000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 4
  *            APB2 Prescaler                 = 2
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 336
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 5
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  
  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }

  /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
  if (HAL_GetREVID() == 0x1001)
  {
    /* Enable the Flash prefetch */
    __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
  }
}

/**
  * @brief  Initialize the USART related hardware for low-level hardware.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
void initUSART_LL(void)
{
	/* Put the USART peripheral in the Asynchronous mode (UART Mode) */
  /* UART1 configured as follow:
      - Word Length = 8 Bits
      - Stop Bit = Two Stop bit
      - Parity = None
      - BaudRate = 9600 baud
      - Hardware flow control disabled (RTS and CTS signals) */
  llUartHandle.Instance          = USART_LL;
  
  llUartHandle.Init.BaudRate     = 115200;
  //llUartHandle.Init.WordLength   = UART_WORDLENGTH_8B;
	llUartHandle.Init.WordLength   = UART_WORDLENGTH_9B;
  llUartHandle.Init.StopBits     = UART_STOPBITS_1;
	//llUartHandle.Init.StopBits     = UART_STOPBITS_2;
  //llUartHandle.Init.Parity       = UART_PARITY_NONE;
	llUartHandle.Init.Parity       = UART_PARITY_EVEN;
  llUartHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
  llUartHandle.Init.Mode         = UART_MODE_TX_RX;
  llUartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
    
  if(HAL_UART_Init(&llUartHandle) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  Initialize the USART related hardware for debug-print.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
static void initUSART_DP(void)
{
	/* UART for debug-print configured as follow:
      - Word Length = 8 Bits
      - Stop Bit = Two Stop bit
      - Parity = None
      - BaudRate = 9600 baud
      - Hardware flow control disabled (RTS and CTS signals) */
  dpUartHandle.Instance          = USART_DP;
  
  dpUartHandle.Init.BaudRate     = 115200;
  dpUartHandle.Init.WordLength   = UART_WORDLENGTH_8B;
  //dpUartHandle.Init.StopBits     = UART_STOPBITS_1;
	dpUartHandle.Init.StopBits     = UART_STOPBITS_2;
  dpUartHandle.Init.Parity       = UART_PARITY_NONE;
	//clUartHandle.Init.Parity       = UART_PARITY_EVEN;
  dpUartHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
  dpUartHandle.Init.Mode         = UART_MODE_TX_RX;
  dpUartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
	
	if(HAL_UART_Init(&dpUartHandle) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  Initialize the USART related hardware for the command line.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
static void initUSART_CL(void)
{
	/* UART for command line configured as follow:
      - Word Length = 8 Bits
      - Stop Bit = Two Stop bit
      - Parity = None
      - BaudRate = 9600 baud
      - Hardware flow control disabled (RTS and CTS signals) */
  clUartHandle.Instance          = USART_CL;
  
  clUartHandle.Init.BaudRate     = 115200;
  clUartHandle.Init.WordLength   = UART_WORDLENGTH_8B;
  clUartHandle.Init.StopBits     = UART_STOPBITS_1;
	//clUartHandle.Init.StopBits     = UART_STOPBITS_2;
  clUartHandle.Init.Parity       = UART_PARITY_NONE;
	//clUartHandle.Init.Parity       = UART_PARITY_EVEN;
  clUartHandle.Init.HwFlowCtl    = UART_HWCONTROL_NONE;
  clUartHandle.Init.Mode         = UART_MODE_TX_RX;
  clUartHandle.Init.OverSampling = UART_OVERSAMPLING_16;
	
	if(HAL_UART_Init(&clUartHandle) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief  Initialize the related hardware for HEAD1_PWM.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
static void initHEAD1(void)
{
	/* GPIOs */
	PROJECT_HEAD1_ENABLE_Init();
	PROJECT_HEAD1_OPTO_Init();
	PROJECT_HEAD1_DIR_Init();
	//PROJECT_HEAD1_PUL_Init();
	
	/* Timer */
	/* PWM */
	uwPrescaler_HEAD1PWM = (uint32_t) ((SystemCoreClock /2) / 1000000) - 1;
	TimHandle_HEAD1PWM.Instance           = TIM_HEAD1_PWM;
	TimHandle_HEAD1PWM.Init.Period        = 2500 - 1;
  TimHandle_HEAD1PWM.Init.Prescaler     = uwPrescaler_HEAD1PWM;
  TimHandle_HEAD1PWM.Init.ClockDivision = 0;
  TimHandle_HEAD1PWM.Init.CounterMode   = TIM_COUNTERMODE_UP;
	
	if ( HAL_OK != HAL_TIM_PWM_Init(&TimHandle_HEAD1PWM))
	{
		/* Starting Error */
    Error_Handler();
	}
	
	TimHEAD1PWM_OCInit.OCMode     = TIM_HEAD1_PWM_OC_MODE;
	TimHEAD1PWM_OCInit.OCPolarity = TIM_OCPOLARITY_LOW;
	TimHEAD1PWM_OCInit.Pulse      = ((TimHandle_HEAD1PWM.Init.Period + 1) * 50) / 100 - 1; /* 50% duty cycle */
	
	HAL_TIM_PWM_ConfigChannel(&TimHandle_HEAD1PWM, &TimHEAD1PWM_OCInit, TIM_HEAD1_PWM_CHANNEL);
	
	/* project control initialization */
	
	smrHEAD1PWM.haveMaxAngle      = 0;
	smrHEAD1PWM.haveMinAngle      = 0;
	smrHEAD1PWM.maxAngle          = (int16_t)(PROJECT_HEAD1_PWM_MAX_ANGLE * 1000);
	smrHEAD1PWM.minAngle          = (int16_t)(PROJECT_HEAD1_PWM_MIN_ANGLE * 1000);
	smrHEAD1PWM.anglePerStep      = PROJECT_HEAD1_PWM_ANGLE_PER_STEP * 1000;
	smrHEAD1PWM.isZeroPointSet    = 0;           /* test use */
	smrHEAD1PWM.anlgeLimiter_1    =  PROJECT_PI; /* test use */
	smrHEAD1PWM.anlgeLimiter_2    = -PROJECT_PI; /* test use */
	smrHEAD1PWM.currentAngle      = 0;           /* test use */
	smrHEAD1PWM.currentAngleF     = 0.0;         /* test use */
	smrHEAD1PWM.targetAngle       = 0;
	smrHEAD1PWM.reverseCounter    = 0;
	smrHEAD1PWM.direction         = 1;
	smrHEAD1PWM.directionMirror   = PROJECT_HEAD_MOTOR_DIR_NON_MIRROR;
	smrHEAD1PWM.upperLimitReached = 0;
	smrHEAD1PWM.lowerLimitReached = 0;
	
	smrHEAD1PWM.timerHandle  = &TimHandle_HEAD1PWM;
	smrHEAD1PWM.PWMChannel   = TIM_HEAD1_PWM_CHANNEL;
	smrHEAD1PWM.DIR_GPIOPort = HEAD1_DIR_GPIO_PORT;
	smrHEAD1PWM.DIR_GPIOPin  = HEAD1_DIR_PIN;
	
	smrHEAD1PWM.counter              = 0;
	smrHEAD1PWM.isSearchingZeroPoint = 0;
	
	smrHEAD1PWM.id = 0; /* zero based */
	smrHEAD1PWM.stopRequest = PROJECT_FLAG_NOT_SET;
	
	if ( PROJECT_OK != setPSMR_HEAD1PWM(&smrHEAD1PWM) )
	{
		/* error */
		Error_Handler();
	}
}

/**
  * @brief  Initialize the related hardware for HEAD2_PWM.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
static void initHEAD2(void)
{
	/* GPIOs */
	PROJECT_HEAD2_ENABLE_Init();
	PROJECT_HEAD2_OPTO_Init();
	PROJECT_HEAD2_DIR_Init();
	//PROJECT_HEAD1_PUL_Init();
	
	/* Timer */
	/* PWM */
	uwPrescaler_HEAD2PWM = (uint32_t) ((SystemCoreClock /2) / 1000000) - 1;
	TimHandle_HEAD2PWM.Instance           = TIM_HEAD2_PWM;
	TimHandle_HEAD2PWM.Init.Period        = 2500 - 1;
  TimHandle_HEAD2PWM.Init.Prescaler     = uwPrescaler_HEAD2PWM;
  TimHandle_HEAD2PWM.Init.ClockDivision = 0;
  TimHandle_HEAD2PWM.Init.CounterMode   = TIM_COUNTERMODE_UP;
	
	if ( HAL_OK != HAL_TIM_PWM_Init(&TimHandle_HEAD2PWM))
	{
		/* Starting Error */
    Error_Handler();
	}
	
	TimHEAD2PWM_OCInit.OCMode     = TIM_HEAD2_PWM_OC_MODE;
	TimHEAD2PWM_OCInit.OCPolarity = TIM_OCPOLARITY_LOW;
	TimHEAD2PWM_OCInit.Pulse      = ((TimHandle_HEAD2PWM.Init.Period + 1) * 50) / 100 - 1; /* 50% duty cycle */
	
	HAL_TIM_PWM_ConfigChannel(&TimHandle_HEAD2PWM, &TimHEAD2PWM_OCInit, TIM_HEAD2_PWM_CHANNEL);
	
	/* project control initialization */
	
	smrHEAD2PWM.haveMaxAngle      = 0;
	smrHEAD2PWM.haveMinAngle      = 0;
	smrHEAD2PWM.maxAngle          = (int16_t)(PROJECT_HEAD2_PWM_MAX_ANGLE * 1000);
	smrHEAD2PWM.minAngle          = (int16_t)(PROJECT_HEAD2_PWM_MIN_ANGLE * 1000);
	smrHEAD2PWM.anglePerStep      = PROJECT_HEAD2_PWM_ANGLE_PER_STEP * 1000;
	smrHEAD2PWM.isZeroPointSet    = 0;           /* test use */
	smrHEAD2PWM.anlgeLimiter_1    =  PROJECT_PI; /* test use */
	smrHEAD2PWM.anlgeLimiter_2    = -PROJECT_PI; /* test use */
	smrHEAD2PWM.currentAngle      = 0;           /* test use */
	smrHEAD2PWM.currentAngleF     = 0.0;         /* test use */
	smrHEAD2PWM.targetAngle       = 0;
	smrHEAD2PWM.reverseCounter    = 0;
	smrHEAD2PWM.direction         = 1;
	smrHEAD2PWM.directionMirror   = PROJECT_HEAD_MOTOR_DIR_MIRROR;
	smrHEAD2PWM.upperLimitReached = 0;
	smrHEAD2PWM.lowerLimitReached = 0;
	
	smrHEAD2PWM.timerHandle  = &TimHandle_HEAD2PWM;
	smrHEAD2PWM.PWMChannel   = TIM_HEAD2_PWM_CHANNEL;
	smrHEAD2PWM.DIR_GPIOPort = HEAD2_DIR_GPIO_PORT;
	smrHEAD2PWM.DIR_GPIOPin  = HEAD2_DIR_PIN;
	
	smrHEAD2PWM.counter              = 0;
	smrHEAD2PWM.isSearchingZeroPoint = 0;
	
	smrHEAD2PWM.id = 1; /* zero based */
	smrHEAD2PWM.stopRequest = PROJECT_FLAG_NOT_SET;
	
	if ( PROJECT_OK != setPSMR_HEAD2PWM(&smrHEAD2PWM) )
	{
		/* error */
		Error_Handler();
	}
}

/**
  * @brief  Initialize the related hardware for super sonic sensor.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
static void initSSONIC(void)
{
	/* Compute the prescaler value to have TIM3 counter clock equal to 1 MHz */
  uwPrescaler_SSONIC = (uint32_t) ((SystemCoreClock /2) / 1000000) - 1;
  
  /* Set TIMx instance */
  TimHandle_SSONIC.Instance = TIM_SSONIC;
   
  /* Initialize TIM3 peripheral as follow:
       + Period = 10000 - 1
       + Prescaler = ((SystemCoreClock/2)/1000000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandle_SSONIC.Init.Period             = 100 - 1;
  TimHandle_SSONIC.Init.Prescaler          = uwPrescaler_SSONIC;
  TimHandle_SSONIC.Init.ClockDivision      = 0;
  TimHandle_SSONIC.Init.CounterMode        = TIM_COUNTERMODE_UP;
  if(HAL_TIM_Base_Init(&TimHandle_SSONIC) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
	
	SSONIC_1.hardwareHandle   = &TimHandle_SSONIC;
	SSONIC_1.GPIOPort         = SSONIC_GPIO_PORT;
	SSONIC_1.GPIOPin          = SSONIC_PIN;
	SSONIC_1.GPIOPin_Detect   = TIM_SSONIC_DETECT_PIN;
	SSONIC_1.distance         = 0;
	SSONIC_1.preSetDelay      = 1;
	SSONIC_1.preSetDelayLoops = 20;
	SSONIC_1.id               = 0;
	
	SSONIC_2.hardwareHandle   = &TimHandle_SSONIC;
	SSONIC_2.GPIOPort         = SSONIC_2_GPIO_PORT;
	SSONIC_2.GPIOPin          = SSONIC_2_PIN;
	SSONIC_2.GPIOPin_Detect   = TIM_SSONIC_2_DETECT_PIN;
	SSONIC_2.distance         = 0;
	SSONIC_2.preSetDelay      = 1;
	SSONIC_2.preSetDelayLoops = 20;
	SSONIC_2.id               = 1;
	
	SSONIC_3.hardwareHandle   = &TimHandle_SSONIC;
	SSONIC_3.GPIOPort         = SSONIC_3_GPIO_PORT;
	SSONIC_3.GPIOPin          = SSONIC_3_PIN;
	SSONIC_3.GPIOPin_Detect   = TIM_SSONIC_3_DETECT_PIN;
	SSONIC_3.distance         = 0;
	SSONIC_3.preSetDelay      = 1;
	SSONIC_3.preSetDelayLoops = 20;
	SSONIC_3.id               = 2;
}

/**
  * @brief  Initialize the related hardware for the detector of the super sonic sensor.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
static void initSSONIC_DETECT(void)
{
	/* Compute the prescaler value to have TIM3 counter clock equal to 1 MHz */
  uwPrescaler_SSONIC_DETECT = (uint32_t) ((SystemCoreClock /2) / PROJECT_SSONIC_DETECT_FREQ) - 1;
  
  /* Set TIM_SSONIC_DETECT instance */
  TimHandle_SSONIC_DETECT.Instance = TIM_SSONIC_DETECT;
   
  /* Initialize TIM3 peripheral as follow:
       + Period = 10000 - 1
       + Prescaler = ((SystemCoreClock/2)/1000000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandle_SSONIC_DETECT.Init.Period             = 100 - 1;
  TimHandle_SSONIC_DETECT.Init.Prescaler          = uwPrescaler_SSONIC_DETECT;
  TimHandle_SSONIC_DETECT.Init.ClockDivision      = 0;
  TimHandle_SSONIC_DETECT.Init.CounterMode        = TIM_COUNTERMODE_UP;
  if(HAL_TIM_Base_Init(&TimHandle_SSONIC_DETECT) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

/**
  * @brief  Initialize the related hardware for the loop timer of the super sonic sensor.
  * @param  None. 
  * @note   None.
  * @retval None.
  */
static void initSSONIC_LOOP(void)
{
	/* Compute the prescaler value to have TIM_SSONIC_LOOP counter clock equal to 10 kHz */
  uwPrescaler_SSONIC_LOOP = (uint32_t) ((SystemCoreClock /2) / PROJECT_SSONIC_LOOP_FREQ) - 1;
  
  /* Set TIM_SSONIC_LOOP instance */
  TimHandle_SSONIC_LOOP.Instance = TIM_SSONIC_LOOP;
   
  /* Initialize TIM_SSONIC_LOOP peripheral as follow:
       + Period = 33333 - 1
       + Prescaler = ((SystemCoreClock/2)/1000000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandle_SSONIC_LOOP.Init.Period             = PROJECT_SSONIC_LOOP_PERIOD - 1;
  TimHandle_SSONIC_LOOP.Init.Prescaler          = uwPrescaler_SSONIC_LOOP;
  TimHandle_SSONIC_LOOP.Init.ClockDivision      = 0;
  TimHandle_SSONIC_LOOP.Init.CounterMode        = TIM_COUNTERMODE_UP;
  if(HAL_TIM_Base_Init(&TimHandle_SSONIC_LOOP) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }
}

/**
  * @brief  Initialize the related hardware for the magnatic sensors.
  * @param  magsArrayHead: the address of the first element of the array holding the mags representations. 
  * @param  n: the number of elements in the array.
  * @note   None.
  * @retval None.
  */
static void initMagSensors(ProjectMagSRepresentation_t* magsArrayHead, uint8_t n)
{
	uint8_t i = 0;
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	if (NULL == magsArrayHead || 0 == n)
	{
		return;
	}
	
	/* enable the clock for the GPIOs for the magnetic sensors */
	MAG_1_GPIO_CLK_ENABLE();
	
//	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
//	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
	
	for (i=0;i<n;i++)
	{
		/* initialize the hardware of the magnetic sensor */
		GPIO_InitStruct.Pin  = (*(magsArrayHead+i)).GPIOPin;
		
		HAL_GPIO_Init((*(magsArrayHead+i)).GPIOPort, &GPIO_InitStruct);
	  HAL_GPIO_WritePin((*(magsArrayHead+i)).GPIOPort, (*(magsArrayHead+i)).GPIOPin, GPIO_PIN_RESET);
		
		HAL_NVIC_SetPriority((*(magsArrayHead+i)).GPIOEXTI_IRQn, 3, 0);
	  HAL_NVIC_EnableIRQ((*(magsArrayHead+i)).GPIOEXTI_IRQn);
	}
}

/**
  * @brief  Tx Transfer completed callback
  * @param  uartHandle: UART handle. 
  * @note   Set flag for transmission of USART.
  * @retval None
  */
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *uartHandle)
{
	if (USART_LL == uartHandle->Instance)
	{
		/* Set transmission flag: transfer complete */
		llUartReady = SET;
	}
	else if (USART_CL == uartHandle->Instance)
	{
		/* Set transmission flag: transfer complete */
		clUartReady = SET;
	}
	else if (USART_DP == uartHandle->Instance)
	{
		/* Set transmission flag: transfer complete */
		dpUartReady = SET;
	}
	else
	{
		/* this is an error */
		/* not implemented */
	}
}

/**
  * @brief  Rx Transfer completed callback
  * @param  UartHandle: UART handle
  * @note   Set flag for receiving data from USART.
  * @retval None
  */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *uartHandle)
{
	//BSP_LED_Toggle(LED3);
	
  if (USART_LL == uartHandle->Instance)
	{
		/* Set transmission flag: transfer complete */
		llUartReady_rec = SET;
	}
	else if (USART_CL == uartHandle->Instance)
	{
		if ( SET != clCommandReady )
		{
			/* save the byte in command buffer */
			*(clCommandProjBuffer.buffer + clByteCounter) = clRxProjBuffer.buffer[0];
			clByteCounter++;
			clCommandProjBuffer.contentLen++;
			
			if (COMMAND_LENGTH == clByteCounter)
			{
				clCommandReady = SET;
				clByteCounter  = 0;
			}
			else
			{
				/* restart the interupt */
				if(HAL_UART_Receive_IT(uartHandle, (uint8_t *)(clRxProjBuffer.buffer), 1) != HAL_OK)
				{
					Error_Handler();
				}
			}
		}
		else
		{
		}
	}
	else if (USART_DP == uartHandle->Instance)
	{
		/* Set transmission flag: transfer complete */
		dpUartReady = SET;
	}
	else
	{
		/* this is an error */
	}
}

/**
  * @brief  UART error callbacks
  * @param  uartHandle: UART handle
  * @note   This example shows a simple way to report transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
void HAL_UART_ErrorCallback(UART_HandleTypeDef *uartHandle)
{
	if (USART_LL == uartHandle->Instance)
	{
		/* do nothing for now */
	}
	else if (USART_CL == uartHandle->Instance)
	{
		/* do nothing for now */
	}
	else if (USART_DP == uartHandle->Instance)
	{
		/* do nothing for now */
	}
	else
	{
		/* this is an error */
		/* do nothing for now */
	}
	
  /* Turn LED5 on: Transfer error in reception/transmission process */
  BSP_LED_On(LED5); 
}

/**
  * @brief  Gather information from every super sonic sensor.
  * @param  None.
  * @note   None.
  * @retval None.
  */
static void querySSONICs(void)
{
	uint32_t counter = 0;
	
#ifdef PROJECT_DEBUG
				
	char strBuff[64];	
				
#endif /* PROJECT_DEBUG */
	
	/* stop the TIM_SSONIC_DETECT */
	if (PROJECT_OK != stopSSONIC_DETECT())
	{
		/* error */
		Project_SSONIC_InProgress = PROJECT_NOTHING_IN_PROGRESS;
	}
	else
	{
		timerStatus_SSONIC_DETECT = TIM_SSONIC_DETECT_HALT;
	}
	
	/* check if the previous detection is success */
	if (SET == SSONIC_DETECT_READY)
	{
		/* calculate the distance */
		uwSSONIC_DETECT_Distance = 0;
		
		if (PROJECT_OK != 
			getSSONIC_DETECT_Distance(uwSSONIC_DETECT_Counter,
								PROJECT_SSONIC_DETECT_TIMER_FREQ, PROJECT_SPEED_OF_SOUND, &uwSSONIC_DETECT_Distance)
		)
		{
			/* error */
			Project_SSONIC_InProgress = PROJECT_NOTHING_IN_PROGRESS;
			pPreviousPSSR->distance = 0;
		}
		else
		{
			pPreviousPSSR->distance = uwSSONIC_DETECT_Distance;
			BSP_LED_Toggle(LED6);
			
#ifdef PROJECT_DEBUG
			
			sprintf(strBuff, "SSONIC_%d: Distance is %dmm.", pPreviousPSSR->id, pPreviousPSSR->distance);
			
			WRITE_DEBUG_PRINT(strBuff);
#endif /* PROJECT_DEBUG */
		}
		SSONIC_DETECT_READY = RESET;
	}
	
	/* start new detection */
	
	Project_SSONIC_InProgress = PROJECT_SSONIC_IN_PROGRESS;
  SSONIC_DETECT_GPIO_Rising = RESET;
	
	if ( PROJECT_OK != startSSONIC(pCurrentPSSR) )
	{
		Project_SSONIC_InProgress = PROJECT_NOTHING_IN_PROGRESS;
	}
}

/**
  * @brief  Write raw strings to the debug-print
  * @param  projBuffer: the project buffer holds the string content.
  * @retval none.
  */
static void writeDebugPrint(const char* strDebug, uint32_t* counter)
{
	uint32_t i = 0;
	uint32_t lenDebugString = 0;
	char* tempString = NULL;
	
	/* check the input arguments */
	if (strDebug == NULL)
	{
		/* error */
		Error_Handler();
	}
	/* write the content of the project buffer to debug-print */
	
	/* truncate the buffer for storage of the index */
	if (strlen(strDebug) > DP_BUFFER_SIZE - DP_INDEX_WIDTH - 2)
	{
		lenDebugString = DP_BUFFER_SIZE - DP_INDEX_WIDTH - 2;
	}
	else
	{
		lenDebugString = strlen(strDebug);
	}
	
	/* index */
	
	if (*counter > DP_MAX_LINES)
	{
		*counter = 0;
	}
	
	tempString = (char* )malloc((DP_INDEX_WIDTH+1)*sizeof(char));
	
	sprintf(tempString, "[%04d]" , *counter); /* This is ugly */
	for (i=0;i<DP_INDEX_WIDTH;i++)
	{
		*(dpTempTxBuffer + i) = *(tempString + i);
	}
	free(tempString); tempString = NULL;
	
	for (i=DP_INDEX_WIDTH;i < lenDebugString + DP_INDEX_WIDTH;i++)
	{
		*(dpTempTxBuffer + i) = *(strDebug + i - DP_INDEX_WIDTH); /* This is ugly */
	}
	
	*(dpTempTxBuffer + lenDebugString + DP_INDEX_WIDTH    ) = '\n';
	*(dpTempTxBuffer + lenDebugString + DP_INDEX_WIDTH + 1) = '\0';
	
	if(HAL_UART_Transmit_DMA(&dpUartHandle, (uint8_t*)(dpTempTxBuffer), lenDebugString + DP_INDEX_WIDTH + 2)!= HAL_OK)
  {
    Error_Handler();
  }
  
  /* Wait for the end of the transfer */  
  while (dpUartReady != SET)
  {
  }
	
	dpUartReady = RESET;
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim: TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	if (TIM_SSONIC == htim->Instance)
	{
		if (PROJECT_OK != performCallback_SSONIC(pCurrentPSSR))
		{
			/* this is an error */
		}
	}
	else if (TIM_SSONIC_DETECT == htim->Instance)
	{
		if (PROJECT_OK != performCallback_SSONIC_DETECT(htim))
		{
			/* this is an error */
		}
	}
	else if (TIM_SSONIC_LOOP == htim->Instance)
	{
		if (PROJECT_OK != performCallback_SSONIC_LOOP(htim))
		{
			/* this is an error */
		}
		else
		{
			if (RESET == SSONIC_LOOP_READY)
			{
				pPreviousPSSR = pCurrentPSSR;
				if (&SSONIC_1 == pCurrentPSSR)
				{
					pCurrentPSSR = &SSONIC_2;
					SSONIC_LOOP_READY = SET;
				}
				else if (&SSONIC_2 == pCurrentPSSR)
				{
					pCurrentPSSR = &SSONIC_3;
					SSONIC_LOOP_READY = SET;
				}
				else if (&SSONIC_3 == pCurrentPSSR)
				{
					pCurrentPSSR = &SSONIC_1;
					SSONIC_LOOP_READY = SET;
				}
				else
				{
					/* error */
					SSONIC_LOOP_READY = RESET;
				}
			}
		}
	}
	else if (TIM_HEAD1_PWM == htim->Instance)
	{
		/* do nothing */
	}
	else
	{
		/* error */
	}
}

/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if ( 
		   ( EXTI_SSONIC_DETECT_GPIO_TRIGERED   == currentEXTI &&
  		   pCurrentPSSR->GPIOPin_Detect       == GPIO_Pin      ) ||
	     ( EXTI_SSONIC_2_DETECT_GPIO_TRIGERED == currentEXTI &&
	       pCurrentPSSR->GPIOPin_Detect       == GPIO_Pin    ) || 
	     ( EXTI_SSONIC_3_DETECT_GPIO_TRIGERED == currentEXTI &&
	       pCurrentPSSR->GPIOPin_Detect       == GPIO_Pin    )
	   )
	{
		/* SSONIC_DETECT_GPIO */
		if ( TIM_SSONIC_DETECT_RUNNING == timerStatus_SSONIC_DETECT )
		{
			/* stop the TIM_SSONIC_DETECT */
			if (PROJECT_OK != stopSSONIC_DETECT())
			{
				/* error */
				Project_SSONIC_InProgress = PROJECT_NOTHING_IN_PROGRESS;
			}
			else
			{
				timerStatus_SSONIC_DETECT = TIM_SSONIC_DETECT_HALT;
				SSONIC_DETECT_READY = SET;
			}
		}
		else if ( TIM_SSONIC_DETECT_HALT == timerStatus_SSONIC_DETECT &&
			        PROJECT_SSONIC_IN_PROGRESS == Project_SSONIC_InProgress )
		{
			SSONIC_DETECT_GPIO_Rising = SET;
			
			/* start the TIM_SSONIC_DETECT */
			if (PROJECT_OK != startSSONIC_DETECT())
			{
				/* error */
			}
			else
			{
				timerStatus_SSONIC_DETECT = TIM_SSONIC_DETECT_RUNNING;
			}
		}
		else
		{
			/* error */
		}
		
		BSP_LED_Toggle(LED4);
	}
  else if ( MAG_1_GPIO_TRIGERED == currentEXTI )
	{
		if (magsArray[0].GPIOPin == GPIO_Pin)
		{
			if ( PROJECT_OK != performCallback_MagSensor(&(magsArray[0])) )
			{
				/* error */
			}
		}
		
		BSP_LED_Toggle(LED5);
	}
	else if ( MAG_2_GPIO_TRIGERED == currentEXTI )
	{
		if (magsArray[1].GPIOPin == GPIO_Pin)
		{
			if ( PROJECT_OK != performCallback_MagSensor(&(magsArray[1])) )
			{
				/* error */
			}
		}
		
		BSP_LED_Toggle(LED5);
	}
	else if ( MAG_3_GPIO_TRIGERED == currentEXTI )
	{
		if (magsArray[2].GPIOPin == GPIO_Pin)
		{
			if ( PROJECT_OK != performCallback_MagSensor(&(magsArray[2])) )
			{
				/* error */
			}
		}
		
		BSP_LED_Toggle(LED5);
	}
	else if ( MAG_4_GPIO_TRIGERED == currentEXTI )
	{
		if (magsArray[3].GPIOPin == GPIO_Pin)
		{
			if ( PROJECT_OK != performCallback_MagSensor(&(magsArray[3])) )
			{
				/* error */
			}
		}
		
		BSP_LED_Toggle(LED5);
	}
}

/**
  * @brief  Callback function for the PWM pulse of HEAD1_PWM.
  * @param  htim: The handle of STM32 Timer.
  * @note   None.
  * @retval None.
  */
void HAL_TIM_PWM_PulseFinishedCallback(TIM_HandleTypeDef * htim)
{
	if ( TIM_HEAD1_PWM == htim->Instance )
	{
		if ( 0 == smrHEAD1PWM.reverseCounter ||
			   PROJECT_FLAG_SET == smrHEAD1PWM.stopRequest )
		{
			if (PROJECT_OK != stopPSMR_PWM(&smrHEAD1PWM))
			{
				/* error */
			}
			
			smrHEAD1PWM.stopRequest = PROJECT_FLAG_NOT_SET;
		}
		
		if (1 != smrHEAD1PWM.lock && 0 != smrHEAD1PWM.reverseCounter)
		{
			smrHEAD1PWM.reverseCounter--;
			smrHEAD1PWM.currentAngleF = smrHEAD1PWM.currentAngleF + smrHEAD1PWM.directionAnglePerStep;
		}
		
		if ( PROJECT_OK == isUpperLimitReached_PSMR(&smrHEAD1PWM) )
		{
			if (1 == smrHEAD1PWM.direction)
			{
				if (PROJECT_OK != stopPSMR_PWM(&smrHEAD1PWM))
				{
					/* error */
				}
				
				if (1 == smrHEAD1PWM.isZeroPointSet && 1 != smrHEAD1PWM.isSearchingZeroPoint)
				{
					smrHEAD1PWM.currentAngleF = smrHEAD1PWM.anlgeLimiter_1;
					smrHEAD1PWM.currentAngle  = (int16_t)(smrHEAD1PWM.currentAngleF);
				}
			}
			else if (-1 == smrHEAD1PWM.direction)
			{
				if ( GPIO_PIN_SET == HAL_GPIO_ReadPin(magsArray[0].GPIOPort, magsArray[0].GPIOPin) )
				{
					clearUpperLimitReached_PSMR(&smrHEAD1PWM);
					magsArray[0].reached = PROJECT_MAG_SENSOR_NOT_REACHED;
				}
			}
		}
		else if ( PROJECT_OK == isLowerLimitReached_PSMR(&smrHEAD1PWM) )
		{
			if (-1 == smrHEAD1PWM.direction)
			{
				if (PROJECT_OK != stopPSMR_PWM(&smrHEAD1PWM))
				{
					/* error */
				}
				
				if (1 == smrHEAD1PWM.isZeroPointSet && 1 != smrHEAD1PWM.isSearchingZeroPoint)
				{
					smrHEAD1PWM.currentAngleF = smrHEAD1PWM.anlgeLimiter_2;
					smrHEAD1PWM.currentAngle  = (int16_t)(smrHEAD1PWM.currentAngleF);
				}
			}
			else if (1 == smrHEAD1PWM.direction)
			{
				if ( GPIO_PIN_SET == HAL_GPIO_ReadPin(magsArray[1].GPIOPort, magsArray[1].GPIOPin) )
				{
					clearLowerLimitReached_PSMR(&smrHEAD1PWM);
					magsArray[1].reached = PROJECT_MAG_SENSOR_NOT_REACHED;
				}
			}
		}
	}
	else if (TIM_HEAD2_PWM == htim->Instance)
	{
		if ( 0 == smrHEAD2PWM.reverseCounter ||
			   PROJECT_FLAG_SET == smrHEAD2PWM.stopRequest )
		{
			if (PROJECT_OK != stopPSMR_PWM(&smrHEAD2PWM))
			{
				/* error */
			}
			
			smrHEAD2PWM.stopRequest = PROJECT_FLAG_NOT_SET;
		}
		
		if (1 != smrHEAD2PWM.lock && 0 != smrHEAD2PWM.reverseCounter)
		{
			smrHEAD2PWM.reverseCounter--;
			smrHEAD2PWM.currentAngleF = smrHEAD2PWM.currentAngleF + smrHEAD2PWM.directionAnglePerStep;
		}
		
		if ( PROJECT_OK == isUpperLimitReached_PSMR(&smrHEAD2PWM) )
		{
			if (1 == smrHEAD2PWM.direction)
			{
				if (PROJECT_OK != stopPSMR_PWM(&smrHEAD2PWM))
				{
					/* error */
				}
				
				if (1 == smrHEAD2PWM.isZeroPointSet && 1 != smrHEAD2PWM.isSearchingZeroPoint)
				{
					smrHEAD2PWM.currentAngleF = smrHEAD2PWM.anlgeLimiter_1;
					smrHEAD2PWM.currentAngle  = (int16_t)(smrHEAD2PWM.currentAngleF);
				}
			}
			else if ( -1 == smrHEAD2PWM.direction )
			{
				if ( GPIO_PIN_SET == HAL_GPIO_ReadPin(magsArray[2].GPIOPort, magsArray[2].GPIOPin) )
				{
					clearUpperLimitReached_PSMR(&smrHEAD2PWM);
					magsArray[2].reached = PROJECT_MAG_SENSOR_NOT_REACHED;
				}
			}
		}
		else if ( PROJECT_OK == isLowerLimitReached_PSMR(&smrHEAD2PWM) )
		{
			if (-1 == smrHEAD2PWM.direction)
			{
				if (PROJECT_OK != stopPSMR_PWM(&smrHEAD2PWM))
				{
					/* error */
				}
				
				if (1 == smrHEAD2PWM.isZeroPointSet && 1 != smrHEAD2PWM.isSearchingZeroPoint)
				{
					smrHEAD2PWM.currentAngleF = smrHEAD2PWM.anlgeLimiter_2;
					smrHEAD2PWM.currentAngle  = (int16_t)(smrHEAD2PWM.currentAngleF);
				}
			}
			else if ( 1 == smrHEAD2PWM.direction )
			{
				if ( GPIO_PIN_SET == HAL_GPIO_ReadPin(magsArray[3].GPIOPort, magsArray[3].GPIOPin) )
				{
					clearLowerLimitReached_PSMR(&smrHEAD2PWM);
					magsArray[3].reached = PROJECT_MAG_SENSOR_NOT_REACHED;
				}
			}
		}
		
//		HEAD2PWM_ReverseCounterArray[posReverseCounterArray] = smrHEAD2PWM.reverseCounter;
//    posReverseCounterArray++;
//		smrHEAD2PWM.counter++;
	}
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
static void Error_Handler(void)
{
  /* Turn LED5 on */
	BSP_LED_On(LED3);
	BSP_LED_On(LED4);
	BSP_LED_On(LED5);
	BSP_LED_On(LED6);
	
  while(1)
  {
		BSP_LED_Toggle(LED3);
		BSP_LED_Toggle(LED4);
		BSP_LED_Toggle(LED5);
		BSP_LED_Toggle(LED6);
		
		HAL_Delay(250);
  }
}

static ProjectStatus_t showHeadMotorLimiters(void)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	char strBuffer[128];
	
	sprintf(strBuffer, "HEAD1: upper->%d, lower->%d. HEAD2: upper->%d, lower->%d",
	smrHEAD1PWM.upperLimitReached, smrHEAD1PWM.lowerLimitReached,
	smrHEAD2PWM.upperLimitReached, smrHEAD2PWM.lowerLimitReached);
	
	WRITE_DEBUG_PRINT(strBuffer)
	
	sta = PROJECT_OK;
	
	return sta;
}

/**
  * @brief  Gather information from every super sonic sensor.
	* @param  level: the error level to be generated.
  * @note   None.
  * @retval PROJECT_OK for success.
  */
static ProjectStatus_t testGenerageError(uint32_t level)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	ProjectDebugStringNode_t* pNode = NULL;
	uint32_t i = 0;
	
	char localBuffer[128];
	
	sprintf(localBuffer,"A test error has been generated.");
	
	for (i=0;i<level;i++)
	{
		/* create a new debug string node */
	
		if (PROJECT_OK != createDebugStringNode(&pNode) )
		{
			/* error */
			return sta;
		}

		if ( PROJECT_OK != PROJECT_FILL_DEBUG_STRING_NODE(pNode, localBuffer) )
		{
			/* error */
			return sta;
		}

		appendDebugStringNode(&currentPDSN, pNode);
	}
	
	sta = PROJECT_FAIL;
	
	return sta;
}

/**
  * @brief  Test the function of the head motors.
	* @param  None.
  * @note   None.
  * @retval PROJECT_OK for success.
  */
static ProjectStatus_t testHeadMotors(void)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	uint8_t addr        = 0x00;
	uint8_t func        = 0x00;
	uint16_t cmd[2]     = {0x0000,0x0000};
	uint16_t data[2]    = {0x0000,0x0000};
	uint8_t transferProtocalCheck = 0;
	
	uint8_t dummyCommandLineBuffer_1[16];
	uint8_t dummyCommandLineBuffer_2[16];
	
	ProjectBuffer_t PBuffer_1;
	ProjectBuffer_t PBuffer_2;
	
	ProjectBuffer_t* pPB = NULL; /* project buffer pointer */
	
	uint32_t i = 0;
	
	char strBuffer[64];
	
	/* fill the dummy command line buffers */
	
	/* HEAD1: -pi/4, HEAD2: -pi/2 */
//	dummyCommandLineBuffer_1[0]  = 0x01;
//	dummyCommandLineBuffer_1[1]  = 0x06;
//	dummyCommandLineBuffer_1[2]  = 0x00;
//	dummyCommandLineBuffer_1[3]  = 0x61;
//	dummyCommandLineBuffer_1[4]  = 0xFC;
//	dummyCommandLineBuffer_1[5]  = 0xEF;
//	dummyCommandLineBuffer_1[6]  = 0x00;
//	dummyCommandLineBuffer_1[7]  = 0x62;
//	dummyCommandLineBuffer_1[8]  = 0xF9;
//	dummyCommandLineBuffer_1[9]  = 0xDD;
//	dummyCommandLineBuffer_1[10] = 0x98;
//	dummyCommandLineBuffer_1[11] = 0x98;

	dummyCommandLineBuffer_1[0]  = 0x01;
	dummyCommandLineBuffer_1[1]  = 0x06;
	dummyCommandLineBuffer_1[2]  = 0x00;
	dummyCommandLineBuffer_1[3]  = 0x61;
	dummyCommandLineBuffer_1[4]  = 0xFC;
	dummyCommandLineBuffer_1[5]  = 0xEF;
	dummyCommandLineBuffer_1[6]  = 0x00;
	dummyCommandLineBuffer_1[7]  = 0x62;
	dummyCommandLineBuffer_1[8]  = 0x00;
	dummyCommandLineBuffer_1[9]  = 0x00;
//	dummyCommandLineBuffer_1[10] = 0x1A;
//	dummyCommandLineBuffer_1[11] = 0x91;
	
	/* HEAD1: pi/4, HEAD2: pi/2 */
//	dummyCommandLineBuffer_2[0]  = 0x01;
//	dummyCommandLineBuffer_2[1]  = 0x06;
//	dummyCommandLineBuffer_2[2]  = 0x00;
//	dummyCommandLineBuffer_2[3]  = 0x61;
//	dummyCommandLineBuffer_2[4]  = 0x03;
//	dummyCommandLineBuffer_2[5]  = 0x11;
//	dummyCommandLineBuffer_2[6]  = 0x00;
//	dummyCommandLineBuffer_2[7]  = 0x62;
//	dummyCommandLineBuffer_2[8]  = 0x06;
//	dummyCommandLineBuffer_2[9]  = 0x23;
//	dummyCommandLineBuffer_2[10] = 0x65;
//	dummyCommandLineBuffer_2[11] = 0x33;
	
	dummyCommandLineBuffer_2[0]  = 0x01;
	dummyCommandLineBuffer_2[1]  = 0x06;
	dummyCommandLineBuffer_2[2]  = 0x00;
	dummyCommandLineBuffer_2[3]  = 0x61;
	dummyCommandLineBuffer_2[4]  = 0x03;
	dummyCommandLineBuffer_2[5]  = 0x11;
	dummyCommandLineBuffer_2[6]  = 0x00;
	dummyCommandLineBuffer_2[7]  = 0x62;
	dummyCommandLineBuffer_2[8]  = 0x00;
	dummyCommandLineBuffer_2[9]  = 0x00;
//	dummyCommandLineBuffer_2[10] = 0x27;
//	dummyCommandLineBuffer_2[11] = 0x4A;
	
	PBuffer_1.buffer     = dummyCommandLineBuffer_1;
	PBuffer_1.contentLen = COMMAND_LENGTH;
	PBuffer_1.len        = 16;
	
	PBuffer_2.buffer     = dummyCommandLineBuffer_2;
	PBuffer_2.contentLen = COMMAND_LENGTH;
	PBuffer_2.len        = 16;
	
	addModbusSuffix(&PBuffer_1, 10);
	addModbusSuffix(&PBuffer_2, 10);
	
	for (i=0;i<2;i++)
	{
		/*##-2- select the command */
		if( 0 == i)
		{
			pPB = &PBuffer_1;
		}
		else if ( 1 == i )
		{
			pPB = &PBuffer_2;
		}
		else
		{
			Error_Handler();
			break;
		}
		
		/*##-2- check the communication validity */
		
		sta = checkTransferValidity(pPB, &transferProtocalCheck);
		
		if (PROJECT_OK != sta || transferProtocalCheck != 1)
		{
			/* error */
			Error_Handler();
		}
		
		
		/*##-3- retrieve the command byte */
		
		BSP_LED_Toggle(LED3);
		recCommandCounter++;
			
		sta = retriveCommand(pPB, &addr, &func, cmd, data, 2);
		
		if (PROJECT_OK != sta)
		{
			/* error */
			Error_Handler();
		}
			
		/*##-4- command execution */
		
		/* execute */
		sta = executeCommand(func, cmd, data, 2);
		
		if (PROJECT_OK != sta)
		{
			/* error */
			Error_Handler();
		}
		
		/* delay for a while */
		HAL_Delay(5000);
		
		/* compose the debug-print string */
		sprintf(strBuffer, "H1: %d, H2: %d", smrHEAD1PWM.currentAngle, smrHEAD2PWM.currentAngle);
		WRITE_DEBUG_PRINT(strBuffer);
	}
	
	sta = PROJECT_OK;
	
	return sta;
}

/**
  * @brief  Test the function of the searching zero point of the head motor.
	* @param  None.
  * @note   None.
  * @retval PROJECT_OK for success.
  */
static ProjectStatus_t testHeadMotorSearchForZeroPoint(ProjectStepMotorRepresentation_t *pPSMR)
{
	ProjectStatus_t sta = PROJECT_FAIL;
		
	char strBuffer[64];
	
	if ( PROJECT_OK != actionHeadSearchForZeroPoint(pPSMR) )
	{
		return sta;
	}
	
	sprintf(strBuffer, "smrHEAD%d: angleLimiter_1 = %f", pPSMR->id, pPSMR->anlgeLimiter_1);
	WRITE_DEBUG_PRINT(strBuffer);
	sprintf(strBuffer, "smrHEAD%d: angleLimiter_2 = %f", pPSMR->id, pPSMR->anlgeLimiter_2);
	WRITE_DEBUG_PRINT(strBuffer);
	
	sta = PROJECT_OK;
	
	return sta;
}

/**
  * @brief  Test the function of the searching zero point of the head motor. The command line will be used.
	* @param  None.
  * @note   None.
  * @retval PROJECT_OK for success.
  */
static ProjectStatus_t testHeadMotorSearchForZeroPointWithCommandLine(void)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	uint8_t addr        = 0x00;
	uint8_t func        = 0x00;
	uint16_t cmd[2]     = {0x0000,0x0000};
	uint16_t data[2]    = {0x0000,0x0000};
	uint8_t transferProtocalCheck = 0;
	
	uint8_t dummyCommandLineBuffer_1[16];
	
	ProjectBuffer_t PBuffer_1;
	
	ProjectBuffer_t* pPB = NULL; /* project buffer pointer */
	
	uint32_t i = 0;
	
	char strBuffer[64];
	
	/* fill the dummy command line buffers */
	
	dummyCommandLineBuffer_1[0]  = 0x01;
	dummyCommandLineBuffer_1[1]  = 0x06;
	dummyCommandLineBuffer_1[2]  = 0x00;
	dummyCommandLineBuffer_1[3]  = 0x60;
	dummyCommandLineBuffer_1[4]  = 0x00;
	dummyCommandLineBuffer_1[5]  = 0x01;
	dummyCommandLineBuffer_1[6]  = 0x00;
	dummyCommandLineBuffer_1[7]  = 0x00;
	dummyCommandLineBuffer_1[8]  = 0x00;
	dummyCommandLineBuffer_1[9]  = 0x00;
//	dummyCommandLineBuffer_1[10] = 0x1A;
//	dummyCommandLineBuffer_1[11] = 0x91;
	
	PBuffer_1.buffer     = dummyCommandLineBuffer_1;
	PBuffer_1.contentLen = COMMAND_LENGTH;
	PBuffer_1.len        = 16;
	
	addModbusSuffix(&PBuffer_1, 10);
	
	for (i=0;i<1;i++)
	{
		/*##-2- select the command */
		if( 0 == i)
		{
			pPB = &PBuffer_1;
		}
		else
		{
			Error_Handler();
			break;
		}
		
		/*##-2- check the communication validity */
		
		sta = checkTransferValidity(pPB, &transferProtocalCheck);
		
		if (PROJECT_OK != sta || transferProtocalCheck != 1)
		{
			/* error */
			Error_Handler();
		}
		
		
		/*##-3- retrieve the command byte */
		
		BSP_LED_Toggle(LED3);
		recCommandCounter++;
			
		sta = retriveCommand(pPB, &addr, &func, cmd, data, 2);
		
		if (PROJECT_OK != sta)
		{
			/* error */
			Error_Handler();
		}
			
		/*##-4- command execution */
		
		/* execute */
		sta = executeCommand(func, cmd, data, 2);
		
		if (PROJECT_OK != sta)
		{
			/* error */
			Error_Handler();
		}
		
		/* delay for a while */
		HAL_Delay(5000);
		
		/* compose the debug-print string */
		sprintf(strBuffer, "H1: %d, H2: %d", smrHEAD1PWM.currentAngle, smrHEAD2PWM.currentAngle);
		WRITE_DEBUG_PRINT(strBuffer);
	}
	
	sta = PROJECT_OK;
	
	return sta;
}

/**
  * @brief  Test the function of reading velocity information from the low level hardware.
	* @param  None.
  * @note   None.
  * @retval PROJECT_OK for success.
  */

static ProjectStatus_t testReadVelocityFromLowLevelHardware(void)
{
	ProjectStatus_t sta = PROJECT_FAIL;
#ifdef PROJECT_TEST_RUN
	char strBuffer[64];
	uint16_t internalDelay = 10;
	
	/* send command to activate the wheels */
	
	if ( PROJECT_OK != actionMove(0x0064, 0x0000) )
	{
		WRITE_DEBUG_PRINT("Activation failed.")
		return sta;
	}
	
	HAL_Delay(internalDelay);
	
	/* send command to read information from motor 1 */
	if (PROJECT_OK != sendCommandToLowLevelHardware(0x01, 0x03, 0x0022, 0x0001, PROJECT_FLAG_SET, &projRecBuffer_TestVelLL, 7) )
	{
		WRITE_DEBUG_PRINT("M1 failed.")
	}
	else
	{
		/* prepare debug-print string */
		sprintf(strBuffer, "M1: %02x %02x %02x %02x %02x %02x %02x",
		projRecBuffer_TestVelLL.buffer[0],
		projRecBuffer_TestVelLL.buffer[1],
		projRecBuffer_TestVelLL.buffer[2],
		projRecBuffer_TestVelLL.buffer[3],
		projRecBuffer_TestVelLL.buffer[4],
		projRecBuffer_TestVelLL.buffer[5],
		projRecBuffer_TestVelLL.buffer[6]);
		
		/* debug-print */
		WRITE_DEBUG_PRINT(strBuffer)
	}
	
	HAL_Delay(internalDelay);
	
	/* send command to read information from motor 2 */
	if (PROJECT_OK != sendCommandToLowLevelHardware(0x02, 0x03, 0x0022, 0x0001, PROJECT_FLAG_SET, &projRecBuffer_TestVelLL, 7) )
	{
		WRITE_DEBUG_PRINT("M2 failed.")
		return sta;
	}
	
	/* prepare debug-print string */
	sprintf(strBuffer, "M2: %02x %02x %02x %02x %02x %02x %02x",
	projRecBuffer_TestVelLL.buffer[0],
	projRecBuffer_TestVelLL.buffer[1],
	projRecBuffer_TestVelLL.buffer[2],
	projRecBuffer_TestVelLL.buffer[3],
	projRecBuffer_TestVelLL.buffer[4],
	projRecBuffer_TestVelLL.buffer[5],
	projRecBuffer_TestVelLL.buffer[6]);
	
	/* debug-print */
	WRITE_DEBUG_PRINT(strBuffer)
	
	HAL_Delay(internalDelay);

#endif
	
	sta = PROJECT_OK;
	
	return sta;
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
