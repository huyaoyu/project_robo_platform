/**
  ******************************************************************************
  * @file    ProjectControl.c 
  * @author  HU Yaoyu <huyaoyu@sjtu.edu.cn>
  * @version V0.1
  * @date    05-December-2015
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "ProjectUtils.h"
#include "ProjectControl.h"
#include "Modbus.h"
#include "LowLevel_AQMD3605BLS.h"
#include "DefMotor.h"
#include "DefRobotGeometry.h"

#include <stdlib.h>

extern TIM_HandleTypeDef TimHandle_SSONIC;
extern TIM_HandleTypeDef TimHandle_SSONIC_DETECT;
extern TIM_HandleTypeDef TimHandle_SSONIC_LOOP;
extern TIM_HandleTypeDef TimHandle_HEAD1PWM;

extern uint32_t uwSSONICCounter;
extern uint32_t uwSSONIC_DETECT_Counter;

extern ProjectBuffer_t msgTxBuffer;
extern ProjectBuffer_t msgRxBuffer;

extern UART_HandleTypeDef llUartHandle; /* USART for low-level hardware */
extern __IO ITStatus llUartReady;       /* the status flag for the low-level USART */
extern __IO ITStatus llUartReady_rec;

/* Private defines --------------------------------------------------------- */

//#define PROJECT_WAIT_LOW_LEVEL_HARDWARE

#define PC_TIM_HANDLE_SSONIC (TimHandle_SSONIC)
#define PC_TIM_HANDLE_SSONIC_DETECT (TimHandle_SSONIC_DETECT)
#define PC_TIM_HANDLE_SSONIC_LOOP   (TimHandle_SSONIC_LOOP)
#define PC_TIM_HANDLE_HEAD1PWM (TimHandle_HEAD1PWM)
#define PC_SSONIC_COUNTER (uwSSONICCounter)
#define PC_SSONIC_DETECT_COUNTER (uwSSONIC_DETECT_Counter)

#define PROJECT_LOW_LEVEL_TX_BUFFER  (msgTxBuffer)
#define PROJECT_LOW_LEVEL_RX_BUFFER  (msgRxBuffer)
#define PROJECT_LOW_LEVEL_USART      (llUartHandle)
#define PROJECT_LOW_LEVEL_USART_FLAG (llUartReady)
#define PROJECT_LOW_LEVEL_USART_REC_FLAG (llUartReady_rec)

/* Private variables ------------------------------------------------------- */

uint8_t isInitialized = 0;

uint16_t nPulsesPerRev      = 60 * DEF_MOTOR_NUM_POLES / LL_MOTOR_ALT_CONST * DEF_MOTOR_SPEED_RATIO * 10;
ProjectFloat_t disPerRev    = DEF_MOTOR_DIM_WHEEL * PROJECT_PI; /* uint: m */
ProjectFloat_t nPulsePerMM  = 0.0; /* pulse per mm */

ProjectFlag_t haveLimits_Wheel = PROJECT_FLAG_NOT_SET;

/* HEAD1PWM */

//__IO static uint8_t upperLimitReached_HEAD1PWM = 0; /* 1 for reached */
//__IO static uint8_t lowerLimitReached_HEAD1PWM = 0; /* 1 for reached */
static ProjectStepMotorRepresentation_t* pPSMR_HEAD1PWM = NULL;
static ProjectStepMotorRepresentation_t* pPSMR_HEAD2PWM = NULL;

/* SSONIC */

static uint32_t* SSONIC_1_Distance = NULL; /* uint: mm */
static uint32_t* SSONIC_2_Distance = NULL; /* uint: mm */
static uint32_t* SSONIC_3_Distance = NULL; /* uint: mm */

/* Private functions ------------------------------------------------------- */


/* Function implementations ------------------------------------------------ */
ProjectStatus_t initProjectControl(void)
{
	ProjectStatus_t sta = PROJECT_OK;
	const char* headString = "The head of debug string node stack.";
	
	isInitialized = 0;
	
	nPulsePerMM = nPulsesPerRev / disPerRev / 1000;
	
	if (NULL != projectDSN)
	{
		releaseDebugStringStack(&projectDSN);
	}
	
  if (PROJECT_OK != createDebugStringNode(&projectDSN))
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	projectDSN->debugString = (char*)malloc(sizeof(char) * (strlen(headString) + 1));
	if (NULL == projectDSN->debugString)
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	/* copy the string */
	
	strcpy(projectDSN->debugString, headString);
	
	currentPDSN = projectDSN;
	
	isInitialized = 1;
	
	return sta;
}

ProjectStatus_t
sendCommandToLowLevelHardware(uint8_t addr, uint8_t func,
		uint16_t cmdTx, uint16_t dataTx,
		ProjectFlag_t waitFlag, ProjectBuffer_t* projB, uint32_t recLen)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	uint32_t rxWaitCounter = 0;
	
	uint8_t transferProtocalCheck = 0;
	uint16_t cmdRx  = 0x0000;
	uint16_t dataRx = 0x0000;
	uint8_t addrRx  = 0x00;
	uint8_t funcRx  = 0x00;
	uint32_t dr     = 0;
	
	uint8_t* pb = NULL;
	uint32_t uartErrorCode = 0x00000000;
	
	if ( PROJECT_FLAG_SET == waitFlag )
	{
		if ( PROJECT_OK != isProjectBufferValid(projB) )
		{
			if ( NULL != projB )
			{
				sta = PROJECT_FAIL;
				return sta;
			}
		}
		
		if ( NULL != projB)
		{
			if ( projB->len < recLen )
			{
				sta = PROJECT_FAIL;
				return sta;
			}
		}
		
		if ( recLen == 0 )
		{
			sta = PROJECT_FAIL;
			return sta;
		}
	}
	
	pb = PROJECT_LOW_LEVEL_TX_BUFFER.buffer;
	
	//	/* Try motor */
	*(pb)     = addr;
	*(pb + 1) = func;
	*(pb + 2) = cmdTx >> 8;
	*(pb + 3) = cmdTx;
	*(pb + 4) = dataTx >> 8;
	*(pb + 5) = dataTx;
	
	if ( addModbusSuffix(&PROJECT_LOW_LEVEL_TX_BUFFER, 6) == PROJECT_FAIL )
	{
		/* error */
		sta = PROJECT_FAIL;
	}
	
	PROJECT_LOW_LEVEL_TX_BUFFER.contentLen = 8;
	
//	__HAL_UART_CLEAR_FLAG(&PROJECT_LOW_LEVEL_USART, UART_FLAG_RXNE);
	__HAL_UART_CLEAR_OREFLAG(&PROJECT_LOW_LEVEL_USART);
//	__HAL_UART_FLUSH_DRREGISTER(&PROJECT_LOW_LEVEL_USART);
	
	if ( PROJECT_FLAG_SET == waitFlag )
	{
//		HAL_UART_DMAStop(&PROJECT_LOW_LEVEL_USART);
	}
	
	if(HAL_UART_Transmit_DMA(&PROJECT_LOW_LEVEL_USART,
		(uint8_t*)(PROJECT_LOW_LEVEL_TX_BUFFER.buffer),
	PROJECT_LOW_LEVEL_TX_BUFFER.contentLen)!= HAL_OK)
  {
    /* error */
		sta = PROJECT_FAIL;
		return sta;
  }
	
	/* Wait for the end of the transfer */  
	while (PROJECT_LOW_LEVEL_USART_FLAG != SET)
	{
	}

	PROJECT_LOW_LEVEL_USART_FLAG = RESET;

	/* check if wait for low level hardware is required */
	
	if (PROJECT_FLAG_SET == waitFlag)
	{
		/* Receive data from low level hardware */
		
		if(HAL_UART_Receive_DMA(&PROJECT_LOW_LEVEL_USART,
			(uint8_t *)(PROJECT_LOW_LEVEL_RX_BUFFER.buffer), recLen) != HAL_OK)
		{
			/* error */
			sta = PROJECT_FAIL;
			return sta;
		}
		
//		HAL_UART_Receive(&PROJECT_LOW_LEVEL_USART,
//				(uint8_t *)(PROJECT_LOW_LEVEL_RX_BUFFER.buffer), recLen, 10);
		
		while (PROJECT_LOW_LEVEL_USART_REC_FLAG != SET  && rxWaitCounter < 0x0FFFFF )
		{
			rxWaitCounter++;
//			HAL_Delay(1);
		}
		/* Reset transmission flag */
		PROJECT_LOW_LEVEL_USART_REC_FLAG = RESET;
		
//		HAL_UART_DMAStop(&PROJECT_LOW_LEVEL_USART);
		
		HAL_Delay(10);
		
		if (rxWaitCounter < 0x0FFFFF)
		{
			if ( PROJECT_LOW_LEVEL_USART_REC_FLAG == SET )
			{
				uartErrorCode = HAL_UART_ERROR_NONE;
				uartErrorCode = HAL_UART_GetError(&PROJECT_LOW_LEVEL_USART);
			}
			
			PROJECT_LOW_LEVEL_RX_BUFFER.contentLen = recLen;
			
			/* check validity of the data transfer */
			
			sta = checkTransferValidity(&PROJECT_LOW_LEVEL_RX_BUFFER, &transferProtocalCheck);
			
			if (PROJECT_OK != sta || transferProtocalCheck != 1)
			{
				/* error */
				sta = PROJECT_FAIL;
				return sta;
			}
			
			/* retrive the command and data */
			
//			sta = retriveCommand(&PROJECT_LOW_LEVEL_RX_BUFFER, &addrRx, &funcRx, &cmdRx, &dataRx, 2);
//			
//			if (PROJECT_OK != sta)
//			{
//				/* error */
//				sta = PROJECT_FAIL;
//				return sta;
//			}
//			else
//			{
//				if ( addrRx != addr  ||
//						 funcRx != func  ||
//						 cmdRx  != cmdTx || 
//						 dataRx != dataTx )
//				{
//					/* error */
//					sta = PROJECT_FAIL;
//					return sta;
//				}
//			}

			if ( NULL != projB )
			{
				if ( PROJECT_OK !=  copyProjectBuffer(&PROJECT_LOW_LEVEL_RX_BUFFER, projB) )
				{
					sta = PROJECT_FAIL;
					return sta;
				}
			}
		} /* if ( rxWaitCounter < 20 ) */
		else
		{
			/* warning */
			sta = PROJECT_FAIL; /* for debug use */
		}
	}
	
	return sta;
}

ProjectStatus_t executeCommand(uint8_t func, uint16_t* cmd, uint16_t* data, uint8_t nCmds)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	switch (func)
	{
		case MODEBUS_WRITE_SIGNLE_FUNC:
		{
			if (PROJECT_OK != executeWriteFunc(cmd, data, nCmds) )
			{
				sta = PROJECT_FAIL;
			}
			break;
		}
		case MODEBUS_READ_SIGNLE_FUNC:
		{
			if (PROJECT_OK != executeReadFunc(cmd, data, nCmds) )
			{
				sta = PROJECT_FAIL;
			}
			break;
		}
		default:
		{
			sta = PROJECT_FAIL;
			break;
		}
	}
	
	return sta;
}

ProjectStatus_t executeWriteFunc(uint16_t* cmd, uint16_t* data, uint8_t nCmds)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	uint8_t i = 0;
	uint8_t breakFlag = 0;
	
	/* large switch-case */
	while ( i < nCmds)
	{
		switch ( *(cmd+i) )
		{
			case PP_SELF_DESCRIPTION:
			{
				break;
			}
			case PP_ACTION_ENABLE:
			{
				break;
			}
			case PP_ACTION_MOVING_LINEAR_VELOCITY:
			{
				actionMove(*(data), *(data+1));
				breakFlag = 1;
				break;
			}
			case PP_ACTION_MOVING_ANGULAR_VELOCITY:
			{
				break;
			}
			case PP_HEAD_MOTOR_GO_TO_ZERO_POINT:
			{
				if (0x0001 == *data)
				{
					actionHeadSearchForZeroPoint(pPSMR_HEAD1PWM);
					HAL_Delay(1000);
					actionHeadSearchForZeroPoint(pPSMR_HEAD2PWM);
				}
				breakFlag = 1;
				break;
			}
			case PP_HEAD_MOTOR_1_TARGET_ANGLE:
			{
				actionHead(*(data), *(data+1));
				breakFlag = 1;
				break;
			}
			case PP_HEAD_MOTOR_2_TARGET_ANGLE:
			{
				break;
			}
			case PP_HEAD_MOTOR_1_DELTA_ANGLE:
			{
				actionHeadDelta(*(data), *(data+1));
				breakFlag = 1;
				break;
			}
			case PP_HEAD_MOTOR_2_DELTA_ANGLE:
			{
				break;
			}
			case PP_PRINT_REGISTER_DEFINITION:
			{
				break;
			}
			case PP_TEST_REGISTER:
			{
				break;
			}
			default:
			{
				sta = PROJECT_FAIL;
				break;
			}
		}
		
		if (breakFlag == 1)
		{
			break;
		}
		else
		{
			i++;
		}
	}
	
	
	return sta;
}

ProjectStatus_t executeReadFunc(uint16_t* cmd, uint16_t* data, uint8_t nCmds)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t checkTransferValidity(ProjectBuffer_t* pbuffer, uint8_t* result)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	uint16_t tempCRC16       = 0x0000;
	uint8_t CRC16Code_High   = 0x00; /* higher byte of CRC16 code */
	uint8_t CRC16Code_Low    = 0x00; /* lower byte of CRC16 code */
	uint8_t bufferCRC16_High = 0x00; /* higher byte of CRC16 code from the buffer */
	uint8_t bufferCRC16_Low  = 0x00; /* lower byte of CRC16 code from the buffer */
	
	/* initial input argument check */
	if (NULL == result)
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	*result = 0;
	
	if (NULL == pbuffer)
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	if (PROJECT_OK != isProjectBufferValid(pbuffer))
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	if (pbuffer->contentLen <= 2)
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	/* initial input argument check done */
	
	/* calculate the CRC16 of the buffer */
	
	tempCRC16 = CRC16(pbuffer->buffer, (uint16_t)(pbuffer->contentLen - 2));
	
	/* get the high and low byte of the CRC16 code */
	CRC16Code_High = (uint8_t) ( (tempCRC16 & 0xFF00) >> 8 );
	CRC16Code_Low  = (uint8_t) ( (tempCRC16 & 0x00FF) );
	
	/* get the CRC16 code from the buffer */
	bufferCRC16_Low  = *(pbuffer->buffer + pbuffer->contentLen - 2);
	bufferCRC16_High = *(pbuffer->buffer + pbuffer->contentLen - 1);
	
	/* check CRC16 */
	if ( bufferCRC16_Low  != CRC16Code_Low ||
       bufferCRC16_High != CRC16Code_High	)
	{
		sta = PROJECT_FAIL;
	}
	else
	{
		*result = 1;
	}
	
	return sta;
}

ProjectStatus_t retriveCommand(ProjectBuffer_t* pbuffer, uint8_t* addr, uint8_t* func, uint16_t* cmd, uint16_t* data, uint8_t nCmds)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	uint8_t tempHighByte = 0x00;
	uint8_t tempLowByte  = 0x00;
	uint16_t temp16      = 0x0000;
	
	uint8_t i = 0; /* NOTE: This must be the same size of nCmds */
	
	/* initial input argument check */
	
	if (NULL == addr || NULL == func || NULL == cmd || NULL == data)
	{
		/* error */
		return sta;
	}
	
	*addr = 0x00;
	*func = 0x00;
	*cmd  = 0x0000;
	*data = 0x0000;
	
	if (NULL == pbuffer)
	{
		/* error */
		return sta;
	}
	
	if (PROJECT_OK != isProjectBufferValid(pbuffer))
	{
		/* error */
		return sta;
	}
	
	if (pbuffer->contentLen < 8)
	{
		/* error */
		return sta;
	}
	
	if (nCmds < 0x01 || nCmds > 0x02)
	{
		/* error */
		return sta;
	}
	
	/* initial input argument check done */
	
	/* retrive the address */
	
	*addr = *(pbuffer->buffer + MODEBUS_ADDRESS_BYTE_POS);
	*func = *(pbuffer->buffer + MODEBUS_FUNCTION_BYTE_POS);
	
	/* retrive the command */
	
	for (i = 0; i<0x02; i++)
	{
		tempHighByte = *(pbuffer->buffer + MODEBUS_REGISTER_BYTE_HIGH_POS + i*4);
		tempLowByte  = *(pbuffer->buffer + MODEBUS_REGISTER_BYTE_LOW_POS  + i*4);
		
		temp16 = 0x0000;
		temp16 = tempHighByte;
		temp16 = temp16 << 8;
		temp16 = temp16 | tempLowByte;
		
		*(cmd + i) = temp16;
		
		/* retrive the data */
		
		tempHighByte = *(pbuffer->buffer + MODEBUS_WRITE_REGISTER_BYTE_HIGH_POS + i*4);
		tempLowByte  = *(pbuffer->buffer + MODEBUS_WRITE_REGISTER_BYTE_LOW_POS  + i*4);
		
		temp16 = 0x0000;
		temp16 = tempHighByte;
		temp16 = temp16 << 8;
		temp16 = temp16 | tempLowByte;
		
		*(data + i) = temp16;
	}
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t actionMove(uint16_t linearV, uint16_t angularV)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	int16_t lv = (int16_t)linearV;  /* unint: mm/s */
	int16_t av = (int16_t)angularV; /* 0.001 rad/s */
	
	uint16_t uFreq1 = 0;
	uint16_t uFreq2 = 0;
	
	ProjectFloat_t v1 = 0.0; /* speed of wheel 1 */
	ProjectFloat_t v2 = 0.0; /* speed of wheel 2 */
	
	/* initial input argument check */
	if ( PROJECT_FLAG_SET == haveLimits_Wheel )
	{
		if ( PROJECT_LINEAR_VELOCITY_TOP     < lv || 
		     PROJECT_LINEAR_VELOCITY_BOTTOM  > lv ||
	       PROJECT_ANGULAR_VELOCITY_TOP    < av ||
	       PROJECT_ANGULAR_VELOCITY_BOTTOM > av )
		{
			/* out of range error */
			sta = PROJECT_FAIL;
			return sta;
		}
	}
	
	if (0 == isInitialized)
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	/* intial check done */
	
	v1 = lv + av / 1000.0 * DEF_ROBOT_DIS_WHEELS / 2;
	v2 = lv - av / 1000.0 * DEF_ROBOT_DIS_WHEELS / 2;
	
	uFreq1 = (int16_t)(     v1 * nPulsePerMM);
	uFreq2 = (int16_t)(-1 * v2 * nPulsePerMM);
	
	/* send command to motor driver 1 */
	
	sta = sendCommandToLowLevelHardware(
	LL_MD1_ADDR, LL_MD_MODBUS_WRITE_FUNC_CODE,
	LL_MD_VEL_CLOSED_LOOP_REG, uFreq1,
	PROJECT_FLAG_SET, NULL, 8);
	
	if (PROJECT_OK != sta)
	{
		/* error */
		sta = PROJECT_FAIL;
	}
	
	HAL_Delay(50);
	
	/* send command to motor driver 2 */
	
	sta = sendCommandToLowLevelHardware(
	LL_MD2_ADDR, LL_MD_MODBUS_WRITE_FUNC_CODE,
	LL_MD_VEL_CLOSED_LOOP_REG, uFreq2,
	PROJECT_FLAG_SET, NULL, 8);
	
	
	if (PROJECT_OK != sta)
	{
		/* error */
		sta = PROJECT_FAIL;
	}
	
	return sta;
}

ProjectStatus_t actionHead(uint16_t target1, uint16_t target2)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	int16_t signedTarget1 = target1;
	int16_t signedTarget2 = target2;
	
	/* check HEAD1_PWM's representation */
	
	if (NULL == pPSMR_HEAD1PWM)
	{
		return sta;
	}
	
	/* check HEAD2_PWM's representation */
	
	if (NULL == pPSMR_HEAD2PWM)
	{
		return sta;
	}
	
	/* set targets for head motors */
	if ( PROJECT_OK != setPSMRTarget(pPSMR_HEAD1PWM, signedTarget1) )
	{
		return sta;
	}
	
	if ( PROJECT_OK != setPSMRTarget(pPSMR_HEAD2PWM, signedTarget2) )
	{
		return sta;
	}
	
	/* run the head motors */
	if ( PROJECT_OK != startPSMR_PWM(pPSMR_HEAD1PWM) )
	{
		return sta;
	}
	
	if ( PROJECT_OK != startPSMR_PWM(pPSMR_HEAD2PWM) )
	{
		return sta;
	}
	
	sta = PROJECT_OK;
	return sta;
}

ProjectStatus_t actionHeadDelta(uint16_t target1, uint16_t target2)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	int16_t tempTarget1 = 0x0000;
	int16_t tempTarget2 = 0x0000;
	
	int16_t tempCurrentAngle1 = 0x000;
	int16_t tempCurrentAngle2 = 0x000;
	
	if ( 0x0000 == target1 )
	{
		pPSMR_HEAD1PWM->stopRequest = PROJECT_FLAG_SET;
	}
	
	if ( 0x0000 == target2 )
	{
		pPSMR_HEAD2PWM->stopRequest = PROJECT_FLAG_SET;
	}

	getCurrentAngle_PSMR(pPSMR_HEAD1PWM, &tempCurrentAngle1);
	getCurrentAngle_PSMR(pPSMR_HEAD2PWM, &tempCurrentAngle2);
	
	tempTarget1 = tempCurrentAngle1 + target1;
	tempTarget2 = tempCurrentAngle2 + target2;
	
	if ( PROJECT_OK != actionHead(tempTarget1, tempTarget2) )
	{
		return sta;
	}
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t actionHeadSearchForZeroPoint(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	int16_t tempTarget     = 0x0000;
	int16_t angleLimiter_1 = 0x0000;
	int16_t angleLimiter_2 = 0x0000;
	int32_t localCounter   = 0;
	
	if (PROJECT_OK != isPSMRValid(pPSMR))
	{
		return sta;
	}
	
	if (1 == pPSMR->isSearchingZeroPoint)
	{
		sta = PROJECT_OK;
		return sta;
	}
	
	pPSMR->isSearchingZeroPoint = 1;
	
	/* search for the positive position limiter */
	tempTarget = 0x188C; /* 2pi rad */
	
	pPSMR->reverseCounter        = (int32_t)( (tempTarget - 0) / pPSMR->anglePerStep );
	pPSMR->direction             = 1;
	pPSMR->directionAnglePerStep = pPSMR->direction * pPSMR->anglePerStep;
	pPSMR->currentAngle          = 0;
	pPSMR->currentAngleF         = 0.0;
	
	if (PROJECT_OK != startPSMR_PWM(pPSMR))
	{
		/* error */
		return sta;
	}
	
	while ( 1 != pPSMR->upperLimitReached )
	{
	}
	
	/* limiter reached */
	
	if (PROJECT_OK != stopPSMR_PWM(pPSMR))
	{
		/* error */
		return sta;
	}
	
	HAL_Delay(500);
	
	/* search for the negative position limiter */
	
	tempTarget = 0xE774; /* -2pi rad */
	
	localCounter                 = (int32_t)( (tempTarget - 0) / pPSMR->anglePerStep );
	pPSMR->reverseCounter        = -1*localCounter;
	pPSMR->direction             = -1;
	pPSMR->directionAnglePerStep = pPSMR->direction * pPSMR->anglePerStep;
	pPSMR->currentAngle          = 0;
	pPSMR->currentAngleF         = 0.0;
	
	if (PROJECT_OK != startPSMR_PWM(pPSMR))
	{
		/* error */
		return sta;
	}
	
	while ( 1 != pPSMR->lowerLimitReached )
	{
	}
	
	/* limiter reached */
	
	if (PROJECT_OK != stopPSMR_PWM(pPSMR))
	{
		/* error */
		return sta;
	}
	
	HAL_Delay(500);
	
	pPSMR->anlgeLimiter_2 = pPSMR->currentAngleF / 2;
	pPSMR->anlgeLimiter_1 = -1*pPSMR->anlgeLimiter_2;
	
	/* go back to the zero point */
	tempTarget = (int16_t) (-1*pPSMR->currentAngleF / 2);
	
	pPSMR->reverseCounter        = (int32_t)( (tempTarget - 0) / pPSMR->anglePerStep );
	pPSMR->direction             = 1;
	pPSMR->directionAnglePerStep = pPSMR->direction * pPSMR->anglePerStep;
	pPSMR->currentAngle          = 0;
	pPSMR->currentAngleF         = 0.0;
	
	if (PROJECT_OK != startPSMR_PWM(pPSMR))
	{
		/* error */
		return sta;
	}
	
	while ( 0 != pPSMR->reverseCounter )
	{
	}
	
	if (PROJECT_OK != stopPSMR_PWM(pPSMR))
	{
		/* error */
		return sta;
	}
	
	HAL_Delay(500);
	
	pPSMR->isZeroPointSet = 1;
	pPSMR->currentAngle   = 0;
	pPSMR->currentAngleF  = 0.0;
	
	pPSMR->isSearchingZeroPoint = 0;
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t calculateLinearAngularVel(uint16_t f1, uint16_t f2, int16_t* lv, int16_t* av)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	ProjectFloat_t v1 = 0.0;
	ProjectFloat_t v2 = 0.0;
	
	/* check arguments */
	
	if ( NULL == lv || NULL == av )
	{
		return sta;
	}
	
//	v1 = lv + av / 1000.0 * DEF_ROBOT_DIS_WHEELS / 2;
//	v2 = lv - av / 1000.0 * DEF_ROBOT_DIS_WHEELS / 2;
//	
//	uFreq1 = (int16_t)(     v1 * nPulsePerMM);
//	uFreq2 = (int16_t)(-1 * v2 * nPulsePerMM);
	
	v1 = ((int16_t)f1) / nPulsePerMM;
	v2 = ((int16_t)f2) / (-1*nPulsePerMM);
	
	*(lv) = (int16_t)( (v1+v2)/2 );
	*(av) = (int16_t)( (v1-v2)/2 * 2 / DEF_ROBOT_DIS_WHEELS * 1000.0 );
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t setPSMR_HEAD1PWM(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (PROJECT_OK != isPSMRValid(pPSMR))
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	pPSMR_HEAD1PWM = pPSMR;
	
	return sta;
}

ProjectStatus_t setPSMR_HEAD2PWM(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (PROJECT_OK != isPSMRValid(pPSMR))
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	pPSMR_HEAD2PWM = pPSMR;
	
	return sta;
}

ProjectStatus_t getCurrentAngle_PSMR(ProjectStepMotorRepresentation_t* pPSMR, int16_t* angle)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if ( PROJECT_OK != isPSMRValid(pPSMR) )
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	if ( NULL == angle )
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	pPSMR->currentAngle = (int16_t) (pPSMR->currentAngleF);
	*angle = pPSMR->currentAngle;
	
	return sta;
}

ProjectStatus_t setUpperLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (NULL == pPSMR)
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	pPSMR->upperLimitReached = 1;
	
	return sta;
}

ProjectStatus_t clearUpperLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (NULL == pPSMR)
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	pPSMR->upperLimitReached = 0;
	
	return sta;
}

ProjectStatus_t setLowerLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (NULL == pPSMR)
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	pPSMR->lowerLimitReached = 1;
	
	return sta;
}

ProjectStatus_t clearLowerLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (NULL == pPSMR)
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	pPSMR->lowerLimitReached = 0;
	
	return sta;
}

ProjectStatus_t isUpperLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	if (NULL == pPSMR)
	{
		return sta;
	}
	
	if (1 == pPSMR->upperLimitReached)
	{
		sta = PROJECT_OK;
	}
	
	return sta;
}
ProjectStatus_t isLowerLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	if (NULL == pPSMR)
	{
		return sta;
	}
	
	if (1 == pPSMR->lowerLimitReached)
	{
		sta = PROJECT_OK;
	}
	
	return sta;
}
ProjectStatus_t isEitherLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	/* initial argument check */
	if (PROJECT_OK != isPSMRValid(pPSMR))
	{
		/* error */
		return sta;
	}
	
	if ( 1 == pPSMR->upperLimitReached || 
		   1 == pPSMR->lowerLimitReached )
	{
		sta = PROJECT_OK;
	}
	
	return sta;
}

ProjectStatus_t isPSMRValid(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	/* NULL */
	if (NULL == pPSMR)
	{
		return sta;
	}
	
	if ( 1 == pPSMR->haveMaxAngle && 1 == pPSMR->haveMinAngle )
	{
		if (pPSMR->maxAngle <= pPSMR->minAngle)
		{
			return sta;
		}
	}
	
	if ( 1 == pPSMR->haveMaxAngle )
	{
		if ( pPSMR->currentAngle > pPSMR->maxAngle )
		{
			return sta;
		}
	}
	
	if ( 1 == pPSMR->haveMinAngle )
	{
		if ( pPSMR->currentAngle < pPSMR->minAngle )
		{
			return sta;
		}
	}
	
//	if ( pPSMR->currentAngle > pPSMR->maxAngle || 
//		   pPSMR->currentAngle < pPSMR->minAngle )
//	{
//		return sta;
//	}
	
	if ( 1 == pPSMR->isZeroPointSet )
	{
		if (pPSMR->anlgeLimiter_1 <= pPSMR->anlgeLimiter_2)
		{
			return sta;
		}
	}
	
	if (pPSMR->anglePerStep < 1e-6)
	{
		return sta;
	}
	
	if (NULL == pPSMR->timerHandle)
	{
    return sta;
	}
	
	sta = PROJECT_OK;
	
	return sta;
}

ProjectStatus_t startPSMR_PWM(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	uint8_t clearToGo = 0;
	
	/* initial argument check */
	
	if (PROJECT_FAIL == isPSMRValid(pPSMR))
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	/* check the limit */
	
	if ( PROJECT_OK == isUpperLimitReached_PSMR(pPSMR) )
	{
		if ( -1 == pPSMR->direction )
		{
			clearToGo = 1;
		}
	}
	else if ( PROJECT_OK == isLowerLimitReached_PSMR(pPSMR) )
	{
		if ( 1 == pPSMR->direction )
		{
			clearToGo = 1;
		}
	}
	else if ( 0 == pPSMR->reverseCounter )
	{
		clearToGo = 0;
	}
	else
	{
		clearToGo = 1;
	}
	
	if (1 == clearToGo)
	{
		if ( 1 == pPSMR->direction * pPSMR->directionMirror )
		{
			HAL_GPIO_WritePin(pPSMR->DIR_GPIOPort, pPSMR->DIR_GPIOPin, GPIO_PIN_SET);
		}
		if ( -1 == pPSMR->direction * pPSMR->directionMirror )
		{
			HAL_GPIO_WritePin(pPSMR->DIR_GPIOPort, pPSMR->DIR_GPIOPin, GPIO_PIN_RESET);
		}
		
		if (HAL_OK != HAL_TIM_PWM_Start_IT(pPSMR->timerHandle, pPSMR->PWMChannel))
		{
			sta = PROJECT_FAIL;
		}
	}
	
	return sta;
}

ProjectStatus_t stopPSMR_PWM(ProjectStepMotorRepresentation_t* pPSMR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	/* initial argument check */
	
	if (PROJECT_FAIL == isPSMRValid(pPSMR))
	{
		/* error */
		sta = PROJECT_FAIL;
		return sta;
	}
	
	if (HAL_OK != HAL_TIM_PWM_Stop_IT(pPSMR->timerHandle, pPSMR->PWMChannel))
	{
		sta = PROJECT_FAIL;
	}
	
	return sta;
}

ProjectStatus_t setPSMRTarget(ProjectStepMotorRepresentation_t* pPSMR, int16_t target)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	int32_t localCounter = 0;
	
	/* initial input argument checke */
	
	if (PROJECT_OK != isPSMRValid(pPSMR))
	{
		/* error */
		return sta;
	}
	
	if ( 1 == pPSMR->haveMaxAngle && target > pPSMR->maxAngle )
	{
		pPSMR->targetAngle = pPSMR->maxAngle;
	}
	else if ( 1 == pPSMR->haveMinAngle && target < pPSMR->minAngle )
	{
		pPSMR->targetAngle = pPSMR->minAngle;
	}
	else
	{
		pPSMR->targetAngle = target;
	}
	
	/* calculate reverse counter */
	
	/* calculate currentAngle */
	
	pPSMR->currentAngle = (int16_t) (pPSMR->currentAngleF + 0.0005);
	
	/* lock the currentAngle */
	
	pPSMR->lock = 1;
	
	localCounter = (int32_t)( (pPSMR->targetAngle - pPSMR->currentAngle) / pPSMR->anglePerStep );
	
	if (localCounter < 0)
	{
		pPSMR->direction = -1;
		localCounter = -1*localCounter;
	}
	else
	{
		pPSMR->direction = 1;
	}
	
	pPSMR->reverseCounter = localCounter;
	pPSMR->directionAnglePerStep = pPSMR->direction * pPSMR->anglePerStep;
	
	/* unlock the currentAngle */
	
	pPSMR->lock = 0;
	
	sta = PROJECT_OK;
	return sta;
}

ProjectStatus_t isPSSRValid(ProjectSSONICRepresentation_t* pPSSR)
{
	ProjectStatus_t sta = PROJECT_FAIL;
	
	if (NULL == pPSSR)
	{
		return sta;
	}
	
	if (NULL == pPSSR->hardwareHandle)
	{
		return sta;
	}
	
	if ( PROJECT_SSONIC_MAX_DELAY < pPSSR->preSetDelay * pPSSR->preSetDelayLoops || 
		   PROJECT_SSONIC_MIN_DELAY > pPSSR->preSetDelay * pPSSR->preSetDelayLoops )
	{
		return sta;
	}
	
	sta = PROJECT_OK;
	return sta;
}

ProjectStatus_t startSSONIC(ProjectSSONICRepresentation_t* pPSSR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	PC_SSONIC_COUNTER = 0;
	
	if ( PROJECT_OK != isPSSRValid(pPSSR) )
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	 if(HAL_TIM_Base_Start_IT(pPSSR->hardwareHandle) != HAL_OK)
  {
    /* Starting Error */
    sta = PROJECT_FAIL;
  }
	
	return sta;
}

ProjectStatus_t startSSONIC_DETECT(void)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	PC_SSONIC_DETECT_COUNTER = 0;
	
	/* Start the TIM Base generation in interrupt mode ####################*/
  /* Start Channel1 */
  if(HAL_TIM_Base_Start_IT(&PC_TIM_HANDLE_SSONIC_DETECT) != HAL_OK)
  {
    /* Starting Error */
    sta = PROJECT_FAIL;
  }
	
	return sta;
}

ProjectStatus_t stopSSONIC_DETECT(void)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (HAL_OK != HAL_TIM_Base_Stop_IT(&PC_TIM_HANDLE_SSONIC_DETECT))
	{
		/* error */
		sta = PROJECT_FAIL;
	}
	
	return sta;
}

ProjectStatus_t startSSONIC_LOOP(void)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (HAL_OK != HAL_TIM_Base_Start_IT(&PC_TIM_HANDLE_SSONIC_LOOP))
	{
		/* error */
		sta = PROJECT_FAIL;
	}
	
	return sta;
}

ProjectStatus_t stopSSONIC_LOOP(void)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (HAL_OK != HAL_TIM_Base_Stop_IT(&PC_TIM_HANDLE_SSONIC_LOOP))
	{
		/* error */
		sta = PROJECT_FAIL;
	}
	
	return sta;
}

ProjectStatus_t getSSONIC_DETECT_Distance(uint32_t detectCounter,
                uint32_t timerFreq,
                uint32_t speedOfSound,
                uint32_t* distance)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	ProjectFloat_t distanceFloat = 0.0;
	
	/* initial input argument check */
	
	if (NULL == distance)
	{
		/* error */
		sta = PROJECT_FAIL;
		*distance = 0;
		return sta;
	}
	
	if ( 0 == detectCounter ||
		   0 == timerFreq ||
	     0 == speedOfSound)
	{
		/* error */
		sta = PROJECT_FAIL;
		*distance = 0;
		return sta;
	}
	
	/* initial input argument check done */
	
	distanceFloat = 1.0 * detectCounter / timerFreq * speedOfSound / 2 * 1000;
	
	*distance = (uint32_t)(distanceFloat);
	
	return sta;
}

ProjectStatus_t performCallback_SSONIC(ProjectSSONICRepresentation_t* pPSSR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if (PC_SSONIC_COUNTER == 1)
	{
		HAL_GPIO_TogglePin(pPSSR->GPIOPort, pPSSR->GPIOPin);
	}
	else if (PC_SSONIC_COUNTER  == 3)
	{
		HAL_GPIO_TogglePin(pPSSR->GPIOPort, pPSSR->GPIOPin);
	}
	else if (PC_SSONIC_COUNTER  == 6)
	{
		HAL_TIM_Base_Stop_IT(pPSSR->hardwareHandle);
	}
	
	if (PC_SSONIC_COUNTER != 6)
	{
		PC_SSONIC_COUNTER++;
	}
		
	//BSP_LED_Toggle(LED4);
	
	return sta;
}

ProjectStatus_t performCallback_SSONIC_LOOP(TIM_HandleTypeDef *htim)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	/* Do nothing for now */
	
	return sta;
}

//ProjectStatus_t performCallback_MagSensor(ProjectMagSRepresentation_t* pMSR)
//{
//	ProjectStatus_t sta = PROJECT_OK;
//	
//	if ( 0 == pMSR->id )
//	{
//		if (PROJECT_MAG_SENSOR_REACHED == pMSR->reached && -1 == pPSMR_HEAD1PWM->direction)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_NOT_REACHED;
//			clearUpperLimitReached_PSMR(pPSMR_HEAD1PWM);
//		}
//		else if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached && 1 == pPSMR_HEAD1PWM->direction)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
//			setUpperLimitReached_PSMR(pPSMR_HEAD1PWM);
//			clearLowerLimitReached_PSMR(pPSMR_HEAD1PWM);
//		}
//		else
//		{
//		}
//	}
//	else if ( 1 == pMSR->id)
//	{
//		if (PROJECT_MAG_SENSOR_REACHED == pMSR->reached && 1 == pPSMR_HEAD1PWM->direction)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_NOT_REACHED;
//			clearLowerLimitReached_PSMR(pPSMR_HEAD1PWM);
//		}
//		else if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached && -1 == pPSMR_HEAD1PWM->direction)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
//			setLowerLimitReached_PSMR(pPSMR_HEAD1PWM);
//			clearUpperLimitReached_PSMR(pPSMR_HEAD1PWM);
//		}
//		else
//		{
//		}
//	}
//	else if ( 2 == pMSR->id)
//	{
//		if (PROJECT_MAG_SENSOR_REACHED == pMSR->reached)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_NOT_REACHED;
//			clearUpperLimitReached_PSMR(pPSMR_HEAD2PWM);
//		}
//		else if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
//			setUpperLimitReached_PSMR(pPSMR_HEAD2PWM);
//			clearLowerLimitReached_PSMR(pPSMR_HEAD2PWM);
//		}
//		else
//		{
//		}
//	}
//	else if ( 3 == pMSR->id)
//	{
//		if (PROJECT_MAG_SENSOR_REACHED == pMSR->reached)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_NOT_REACHED;
//			clearLowerLimitReached_PSMR(pPSMR_HEAD2PWM);
//		}
//		else if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached)
//		{
//			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
//			setLowerLimitReached_PSMR(pPSMR_HEAD2PWM);
//			clearUpperLimitReached_PSMR(pPSMR_HEAD2PWM);
//		}
//		else
//		{
//		}
//	}
//	
//	return sta;
//}

ProjectStatus_t performCallback_MagSensor(ProjectMagSRepresentation_t* pMSR)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	if ( 0 == pMSR->id )
	{
		if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached && 1 == pPSMR_HEAD1PWM->direction)
		{
			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
			setUpperLimitReached_PSMR(pPSMR_HEAD1PWM);
			clearLowerLimitReached_PSMR(pPSMR_HEAD1PWM);
		}
		else
		{
		}
	}
	else if ( 1 == pMSR->id)
	{
		if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached && -1 == pPSMR_HEAD1PWM->direction)
		{
			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
			setLowerLimitReached_PSMR(pPSMR_HEAD1PWM);
			clearUpperLimitReached_PSMR(pPSMR_HEAD1PWM);
		}
		else
		{
		}
	}
	else if ( 2 == pMSR->id)
	{
		if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached && 1 == pPSMR_HEAD2PWM->direction)
		{
			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
			setUpperLimitReached_PSMR(pPSMR_HEAD2PWM);
			clearLowerLimitReached_PSMR(pPSMR_HEAD2PWM);
		}
		else
		{
		}
	}
	else if ( 3 == pMSR->id)
	{
		if (PROJECT_MAG_SENSOR_NOT_REACHED == pMSR->reached && -1 == pPSMR_HEAD2PWM->direction)
		{
			pMSR->reached = PROJECT_MAG_SENSOR_REACHED;
			setLowerLimitReached_PSMR(pPSMR_HEAD2PWM);
			clearUpperLimitReached_PSMR(pPSMR_HEAD2PWM);
		}
		else
		{
		}
	}
	
	return sta;
}

ProjectStatus_t performCallback_SSONIC_DETECT(TIM_HandleTypeDef *htim)
{
	ProjectStatus_t sta = PROJECT_OK;
	
	PC_SSONIC_DETECT_COUNTER++;
	
	return sta;
}
