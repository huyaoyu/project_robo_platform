/**
  ******************************************************************************
  * @file    ProjectUtils.c 
  * @author  Hu Yaoyu <huyaoyu@sjtu.edu.cn>
  * @version V0.1
  * @date    28-November-2015
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "ProjectUtils.h"

/** @addtogroup STM32F4xx_HAL_Examples
  * @{
  */

/** @addtogroup UART_TwoBoards_ComDMA
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/* Implementations ---------------------------------------------------------- */
int8_t copyProjectBuffer(ProjectBuffer_t* from, ProjectBuffer_t* to)
{
	int8_t sta = PROJECT_FAIL;
	uint32_t i = 0;
	
#ifdef PROJECT_DEBUG
	/* check the input arguments */
	
	if (isProjectBufferValid(from) == PROJECT_FAIL)
	{
		/* error */
		return sta;
	}
	
	if (isProjectBufferValid(to) == PROJECT_FAIL)
	{
		/* error */
		return sta;
	}
	
	if (from->contentLen > to->len)
	{
		/* error */
		return sta;
	}
	
#endif /* PROJECT_DEBUG */
	
	/* copy operation */
	
	for (i = 0;i < from->contentLen; i++)
	{
		*(to->buffer + i) = *(from->buffer + i);
	}
	to->contentLen = from->contentLen;
	sta = PROJECT_OK;
	
	return sta;
}


ProjectStatus_t splitIntoArray(uint16_t ori, uint8_t* des)
{
	ProjectStatus_t sta  = PROJECT_OK;
	
	if ( NULL == des )
	{
		sta = PROJECT_FAIL;
		return sta;
	}
	
	*(des)   = (uint8_t) (ori >> 8); /* higher byte */
	*(des+1) = (uint8_t) (ori);      /* lower byte */
	
	return sta;
}

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
