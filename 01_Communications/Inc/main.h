/**
  ******************************************************************************
  * @file    main.h 
  * @author  MCD Application Team
  * @version V1.2.3
  * @date    09-October-2015
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4_discovery.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

/* ======= constants for EXTIs ========= */

#define EXTI_NONE_TRIGERED   (255)
#define EXTI_0_TRIGERED      (0)
#define EXTI_1_TRIGERED      (1)
#define EXTI_2_TRIGERED      (2)
#define EXTI_3_TRIGERED      (3)
#define EXTI_4_TRIGERED      (4)
#define EXTI_9_5_TRIGERED    (9)
#define EXTI_15_10_TRIGERED  (15)

/* ====== constants for USART_LL ======= */

/* Definition for low-level hardware USART clock resources */
#define USART_LL                           USART2
#define USART_LL_CLK_ENABLE()              __HAL_RCC_USART2_CLK_ENABLE();
#define DMA_LL_CLK_ENABLE()                __HAL_RCC_DMA1_CLK_ENABLE()
#define USART_LL_RX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE()
#define USART_LL_TX_GPIO_CLK_ENABLE()      __HAL_RCC_GPIOA_CLK_ENABLE() 

#define USART_LL_FORCE_RESET()             __HAL_RCC_USART2_FORCE_RESET()
#define USART_LL_RELEASE_RESET()           __HAL_RCC_USART2_RELEASE_RESET()

/* Definition for USART_LL Pins */
#define USART_LL_TX_PIN                    GPIO_PIN_2
#define USART_LL_TX_GPIO_PORT              GPIOA
#define USART_LL_TX_AF                     GPIO_AF7_USART2
#define USART_LL_RX_PIN                    GPIO_PIN_3
#define USART_LL_RX_GPIO_PORT              GPIOA
#define USART_LL_RX_AF                     GPIO_AF7_USART2

/* Definition for USART_LL's DMA */
#define USART_LL_TX_DMA_CHANNEL            DMA_CHANNEL_4
#define USART_LL_TX_DMA_STREAM             DMA1_Stream6
#define USART_LL_RX_DMA_CHANNEL            DMA_CHANNEL_4
#define USART_LL_RX_DMA_STREAM             DMA1_Stream5

/* Definition for USART_LL's NVIC */
#define USART_LL_DMA_TX_IRQn               DMA1_Stream6_IRQn
#define USART_LL_DMA_RX_IRQn               DMA1_Stream5_IRQn
#define USART_LL_DMA_TX_IRQHandler         DMA1_Stream6_IRQHandler
#define USART_LL_DMA_RX_IRQHandler         DMA1_Stream5_IRQHandler
#define USART_LL_IRQn                      USART2_IRQn
#define USART_LL_IRQHandler                USART2_IRQHandler

/* Size of Transmission buffer */
#define TXBUFFERSIZE                     (COUNTOF(aTxBuffer) - 1)

/* Size of Reception buffer */
#define RXBUFFERSIZE                     TXBUFFERSIZE

/* ====== constants for command line USART ======= */

/* Definition for command line USART clock resources */
#define USART_CL                         USART1
#define USART_CL_CLK_ENABLE()            __HAL_RCC_USART1_CLK_ENABLE();
#define DMA_CL_CLK_ENABLE()              __HAL_RCC_DMA2_CLK_ENABLE()
#define USART_CL_RX_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE()
#define USART_CL_TX_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE() 

#define USART_CL_FORCE_RESET()           __HAL_RCC_USART1_FORCE_RESET()
#define USART_CL_RELEASE_RESET()         __HAL_RCC_USART1_RELEASE_RESET()

/* Definition for command line USART Pins */
#define USART_CL_TX_PIN                  GPIO_PIN_6
#define USART_CL_TX_GPIO_PORT            GPIOB
#define USART_CL_TX_AF                   GPIO_AF7_USART1
#define USART_CL_RX_PIN                  GPIO_PIN_7
#define USART_CL_RX_GPIO_PORT            GPIOB
#define USART_CL_RX_AF                   GPIO_AF7_USART1

/* Definition for command line USART's DMA */
#define USART_CL_TX_DMA_CHANNEL          DMA_CHANNEL_4
#define USART_CL_TX_DMA_STREAM           DMA2_Stream7
#define USART_CL_RX_DMA_CHANNEL          DMA_CHANNEL_4
#define USART_CL_RX_DMA_STREAM           DMA2_Stream5

/* Definition for command line USART's NVIC */
#define USART_CL_DMA_TX_IRQn             DMA2_Stream7_IRQn
#define USART_CL_DMA_RX_IRQn             DMA2_Stream5_IRQn
#define USART_CL_DMA_TX_IRQHandler       DMA2_Stream7_IRQHandler
#define USART_CL_DMA_RX_IRQHandler       DMA2_Stream5_IRQHandler
#define USART_CL_IRQn                    USART1_IRQn
#define USART_CL_IRQHandler              USART1_IRQHandler

/* ====== constants for debug-print USART ======= */

/* Definition for debug-print USART clock resources */
#define USART_DP                         USART3
#define USART_DP_CLK_ENABLE()            __HAL_RCC_USART3_CLK_ENABLE();
#define DMA_DP_CLK_ENABLE()              __HAL_RCC_DMA1_CLK_ENABLE()
#define USART_DP_RX_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE()
#define USART_DP_TX_GPIO_CLK_ENABLE()    __HAL_RCC_GPIOB_CLK_ENABLE() 

#define USART_DP_FORCE_RESET()           __HAL_RCC_USART3_FORCE_RESET()
#define USART_DP_RELEASE_RESET()         __HAL_RCC_USART3_RELEASE_RESET()

/* Definition for debug-print USART Pins */
#define USART_DP_TX_PIN                  GPIO_PIN_10
#define USART_DP_TX_GPIO_PORT            GPIOB
#define USART_DP_TX_AF                   GPIO_AF7_USART3
#define USART_DP_RX_PIN                  GPIO_PIN_11
#define USART_DP_RX_GPIO_PORT            GPIOB
#define USART_DP_RX_AF                   GPIO_AF7_USART3

/* Definition for debug-print USART's DMA */
#define USART_DP_TX_DMA_CHANNEL          DMA_CHANNEL_4
#define USART_DP_TX_DMA_STREAM           DMA1_Stream3
#define USART_DP_RX_DMA_CHANNEL          DMA_CHANNEL_4
#define USART_DP_RX_DMA_STREAM           DMA1_Stream1

/* Definition for debug-print USART's NVIC */
#define USART_DP_DMA_TX_IRQn             DMA1_Stream3_IRQn
#define USART_DP_DMA_RX_IRQn             DMA1_Stream1_IRQn
#define USART_DP_DMA_TX_IRQHandler       DMA1_Stream3_IRQHandler
#define USART_DP_DMA_RX_IRQHandler       DMA1_Stream1_IRQHandler
#define USART_DP_IRQn                    USART3_IRQn
#define USART_DP_IRQHandler              USART3_IRQHandler

/* ====== constants for HEAD1 tim PWM ======= */

/* Definition for TIM_HEAD1_PWM clock resources */
#define TIM_HEAD1_PWM                    TIM4
#define TIM_HEAD1_PWM_CLK_ENABLE         __HAL_RCC_TIM4_CLK_ENABLE
#define TIM_HEAD1_PWM_OC_MODE            TIM_OCMODE_PWM1
#define TIM_HEAD1_PWM_CHANNEL            TIM_CHANNEL_3

/* Definition for TIM_HEAD1_PWM's NVIC */
#define TIM_HEAD1_PWM_IRQn               TIM4_IRQn
#define TIM_HEAD1_PWM_IRQHandler         TIM4_IRQHandler

/* Definition for HEAD1_ENABLE GPIO */
#define HEAD1_ENABLE_PIN                 GPIO_PIN_8
#define HEAD1_ENABLE_GPIO_PORT           GPIOD
#define HEAD1_ENABLE_GPIO_CLK_ENABLE()   __GPIOD_CLK_ENABLE()  
#define HEAD1_ENABLE_GPIO_CLK_DISABLE()  __GPIOD_CLK_DISABLE()

/* Definition for HEAD1_OPTO GPIO */
#define HEAD1_OPTO_PIN                   GPIO_PIN_9
#define HEAD1_OPTO_GPIO_PORT             GPIOD
#define HEAD1_OPTO_GPIO_CLK_ENABLE()     __GPIOD_CLK_ENABLE()  
#define HEAD1_OPTO_GPIO_CLK_DISABLE()    __GPIOD_CLK_DISABLE() 

/* Definition for HEAD1_DIR GPIO */
#define HEAD1_DIR_PIN                    GPIO_PIN_10
#define HEAD1_DIR_GPIO_PORT              GPIOD
#define HEAD1_DIR_GPIO_CLK_ENABLE()      __GPIOD_CLK_ENABLE()  
#define HEAD1_DIR_GPIO_CLK_DISABLE()     __GPIOD_CLK_DISABLE()

/* Definition for HEAD1_PUL_PWM GPIO */
#define HEAD1_PUL_PWM_PIN                GPIO_PIN_8
#define HEAD1_PUL_PWM_GPIO_PORT          GPIOB
#define HEAD1_PUL_PWM_GPIO_CLK_ENABLE()  __GPIOB_CLK_ENABLE()  
#define HEAD1_PUL_PWM_GPIO_CLK_DISABLE() __GPIOB_CLK_DISABLE()
#define HEAD1_PUL_PWM_GPIO_AF            GPIO_AF2_TIM4


/* ====== constants for HEAD2 tim PWM ======= */

/* Definition for TIM_HEAD2_PWM clock resources */
#define TIM_HEAD2_PWM                    TIM5
#define TIM_HEAD2_PWM_CLK_ENABLE         __HAL_RCC_TIM5_CLK_ENABLE
#define TIM_HEAD2_PWM_OC_MODE            TIM_OCMODE_PWM1
#define TIM_HEAD2_PWM_CHANNEL            TIM_CHANNEL_2

/* Definition for TIM_HEAD2_PWM's NVIC */
#define TIM_HEAD2_PWM_IRQn               TIM5_IRQn
#define TIM_HEAD2_PWM_IRQHandler         TIM5_IRQHandler

/* Definition for HEAD2_ENABLE GPIO */
#define HEAD2_ENABLE_PIN                 GPIO_PIN_1
#define HEAD2_ENABLE_GPIO_PORT           GPIOC
#define HEAD2_ENABLE_GPIO_CLK_ENABLE()   __GPIOC_CLK_ENABLE()  
#define HEAD2_ENABLE_GPIO_CLK_DISABLE()  __GPIOC_CLK_DISABLE()

/* Definition for HEAD2_OPTO GPIO */
#define HEAD2_OPTO_PIN                   GPIO_PIN_3
#define HEAD2_OPTO_GPIO_PORT             GPIOC
#define HEAD2_OPTO_GPIO_CLK_ENABLE()     __GPIOC_CLK_ENABLE()  
#define HEAD2_OPTO_GPIO_CLK_DISABLE()    __GPIOC_CLK_DISABLE() 

/* Definition for HEAD2_DIR GPIO */
#define HEAD2_DIR_PIN                    GPIO_PIN_2
#define HEAD2_DIR_GPIO_PORT              GPIOC
#define HEAD2_DIR_GPIO_CLK_ENABLE()      __GPIOC_CLK_ENABLE()  
#define HEAD2_DIR_GPIO_CLK_DISABLE()     __GPIOC_CLK_DISABLE()

/* Definition for HEAD2_PUL_PWM GPIO */
#define HEAD2_PUL_PWM_PIN                GPIO_PIN_1
#define HEAD2_PUL_PWM_GPIO_PORT          GPIOA
#define HEAD2_PUL_PWM_GPIO_CLK_ENABLE()  __GPIOA_CLK_ENABLE()  
#define HEAD2_PUL_PWM_GPIO_CLK_DISABLE() __GPIOA_CLK_DISABLE()
#define HEAD2_PUL_PWM_GPIO_AF            GPIO_AF2_TIM5


/* ====== constants for SSONIC tim ======= */

/* Definition for TIM_SSONIC clock resources */
#define TIM_SSONIC                       TIM2
#define TIM_SSONIC_CLK_ENABLE            __HAL_RCC_TIM2_CLK_ENABLE

/* Definition for TIM_SSONIC's NVIC */
#define TIM_SSONIC_IRQn                  TIM2_IRQn
#define TIM_SSONIC_IRQHandler            TIM2_IRQHandler

/* Definition for SSONIC GPIO */
#define SSONIC_PIN                       GPIO_PIN_1
#define SSONIC_GPIO_PORT                 GPIOB
#define SSONIC_GPIO_CLK_ENABLE()         __GPIOB_CLK_ENABLE()  
#define SSONIC_GPIO_CLK_DISABLE()        __GPIOB_CLK_DISABLE()
//#define SSONIC_GPIO_AF

/* Definition for TIM_SSONIC_DETECT clock resources */
#define TIM_SSONIC_DETECT                TIM7
#define TIM_SSONIC_DETECT_CLK_ENABLE     __HAL_RCC_TIM7_CLK_ENABLE

/* Definition for TIM_SSONIC_DETECT's NVIC */
#define TIM_SSONIC_DETECT_IRQn           TIM7_IRQn
#define TIM_SSONIC_DETECT_IRQHandler     TIM7_IRQHandler
#define TIM_SSONIC_DETECT_RUNNING        (1)
#define TIM_SSONIC_DETECT_HALT           (0)

/* Definition for TIM_SSONIC_DETECT_GPIO clock resources */
#define TIM_SSONIC_DETECT_PIN                GPIO_PIN_0
#define TIM_SSONIC_DETECT_GPIO_PORT          GPIOB
#define TIM_SSONIC_DETECT_GPIO_CLK_ENABLE()  __GPIOB_CLK_ENABLE()
#define TIM_SSONIC_DETECT_GPIO_CLK_DISABLE() __GPIOB_CLK_DISABLE()

/* Definition for TIM_SSONIC_DETECT_GPIO's NVIC */
#define TIM_SSONIC_DETECT_GPIO_IRQn          EXTI0_IRQn
#define TIM_SSONIC_DETECT_GPIO_IRQHandler    EXTI0_IRQHandler
#define EXTI_SSONIC_DETECT_GPIO_TRIGERED     EXTI_0_TRIGERED
#define EXTI_SSONIC_2_DETECT_GPIO_TRIGERED   EXTI_1_TRIGERED
#define EXTI_SSONIC_3_DETECT_GPIO_TRIGERED   EXTI_2_TRIGERED


/* SSONIC_2 */
/* Definition for SSONIC GPIO */
#define SSONIC_2_PIN                       GPIO_PIN_0
#define SSONIC_2_GPIO_PORT                 GPIOD
#define SSONIC_2_GPIO_CLK_ENABLE()         __GPIOD_CLK_ENABLE()  
#define SSONIC_2_GPIO_CLK_DISABLE()        __GPIOD_CLK_DISABLE()

/* Definition for TIM_SSONIC_DETECT_GPIO clock resources */
#define TIM_SSONIC_2_DETECT_PIN                GPIO_PIN_1
#define TIM_SSONIC_2_DETECT_GPIO_PORT          GPIOD
#define TIM_SSONIC_2_DETECT_GPIO_CLK_ENABLE()  __GPIOD_CLK_ENABLE()
#define TIM_SSONIC_2_DETECT_GPIO_CLK_DISABLE() __GPIOD_CLK_DISABLE()

#define TIM_SSONIC_2_DETECT_GPIO_IRQn          EXTI1_IRQn
#define TIM_SSONIC_2_DETECT_GPIO_IRQHandler    EXTI1_IRQHandler
#define EXTI_SSONIC_2_DETECT_GPIO_TRIGERED     EXTI_1_TRIGERED


/* SSONIC_3 */
/* Definition for SSONIC GPIO */
#define SSONIC_3_PIN                       GPIO_PIN_3
#define SSONIC_3_GPIO_PORT                 GPIOD
#define SSONIC_3_GPIO_CLK_ENABLE()         __GPIOD_CLK_ENABLE()  
#define SSONIC_3_GPIO_CLK_DISABLE()        __GPIOD_CLK_DISABLE()

/* Definition for TIM_SSONIC_DETECT_GPIO clock resources */
#define TIM_SSONIC_3_DETECT_PIN                GPIO_PIN_2
#define TIM_SSONIC_3_DETECT_GPIO_PORT          GPIOD
#define TIM_SSONIC_3_DETECT_GPIO_CLK_ENABLE()  __GPIOD_CLK_ENABLE()
#define TIM_SSONIC_3_DETECT_GPIO_CLK_DISABLE() __GPIOD_CLK_DISABLE()

#define TIM_SSONIC_3_DETECT_GPIO_IRQn          EXTI2_IRQn
#define TIM_SSONIC_3_DETECT_GPIO_IRQHandler    EXTI2_IRQHandler
#define EXTI_SSONIC_3_DETECT_GPIO_TRIGERED     EXTI_2_TRIGERED


/* SSONIC loop timer */

/* Definition for SSONIC_LOOP */
#define TIM_SSONIC_LOOP                        TIM3
#define TIM_SSONIC_LOOP_CLK_ENABLE             __HAL_RCC_TIM3_CLK_ENABLE
#define TIM_SSONIC_LOOP_IRQn                   TIM3_IRQn
#define TIM_SSONIC_LOOP_IRQHandler             TIM3_IRQHandler


/* constants for magnetic sensors */
/* magnetic sensor 1 */
#define MAG_1_PIN                            GPIO_PIN_4
#define MAG_1_GPIO_PORT                      GPIOE
#define MAG_1_GPIO_CLK_ENABLE()              __GPIOE_CLK_ENABLE()
#define MAG_1_GPIO_CLK_DISABLE()             __GPIOE_CLK_DISABLE()

#define MAG_1_GPIO_IRQn                      EXTI4_IRQn
#define MAG_1_GPIO_IRQHandler                EXTI4_IRQHandler
#define MAG_1_GPIO_TRIGERED                  EXTI_4_TRIGERED

/* magnetic sensor 2 */
#define MAG_2_PIN                            GPIO_PIN_3
#define MAG_2_GPIO_PORT                      GPIOE
#define MAG_2_GPIO_CLK_ENABLE()              __GPIOE_CLK_ENABLE()
#define MAG_2_GPIO_CLK_DISABLE()             __GPIOE_CLK_DISABLE()

#define MAG_2_GPIO_IRQn                      EXTI3_IRQn
#define MAG_2_GPIO_IRQHandler                EXTI3_IRQHandler
#define MAG_2_GPIO_TRIGERED                  EXTI_3_TRIGERED

/* magnetic sensor 3 */
#define MAG_3_PIN                            GPIO_PIN_5
#define MAG_3_GPIO_PORT                      GPIOE
#define MAG_3_GPIO_CLK_ENABLE()              __GPIOE_CLK_ENABLE()
#define MAG_3_GPIO_CLK_DISABLE()             __GPIOE_CLK_DISABLE()

#define MAG_3_GPIO_IRQn                      EXTI9_5_IRQn
#define MAG_3_GPIO_IRQHandler                EXTI9_5_IRQHandler
#define MAG_3_GPIO_TRIGERED                  EXTI_9_5_TRIGERED

/* magnetic sensor 4 */
#define MAG_4_PIN                            GPIO_PIN_11
#define MAG_4_GPIO_PORT                      GPIOC
#define MAG_4_GPIO_CLK_ENABLE()              __GPIOC_CLK_ENABLE()
#define MAG_4_GPIO_CLK_DISABLE()             __GPIOC_CLK_DISABLE()

#define MAG_4_GPIO_IRQn                      EXTI15_10_IRQn
#define MAG_4_GPIO_IRQHandler                EXTI15_10_IRQHandler
#define MAG_4_GPIO_TRIGERED                  EXTI_15_10_TRIGERED

  
/* Exported macro ------------------------------------------------------------*/
#define COUNTOF(__BUFFER__)   (sizeof(__BUFFER__) / sizeof(*(__BUFFER__)))

/* Exported functions ------------------------------------------------------- */

/* Functions for HEAD1 */
void PROJECT_HEAD1_ENABLE_Init(void);
void PROJECT_HEAD1_DIR_Init(void);
void PROJECT_HEAD1_OPTO_Init(void);
//void PROJECT_HEAD1_PUL_Init(void);

/* Functions for HEAD2 */
void PROJECT_HEAD2_ENABLE_Init(void);
void PROJECT_HEAD2_DIR_Init(void);
void PROJECT_HEAD2_OPTO_Init(void);

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
