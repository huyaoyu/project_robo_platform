/**
  ******************************************************************************
  * @file    ProjectControl.h 
  * @author  MCD Application Team
  * @version V1.2.3
  * @date    05-December-2015
  * @brief   Header for main.c module
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PROJECT_CONTROL_H
#define __PROJECT_CONTROL_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "CommonHeader.h"

/* Exported types ------------------------------------------------------------*/

#define PROJECT_HEAD_MOTOR_DIR_MIRROR     (-1)
#define PROJECT_HEAD_MOTOR_DIR_NON_MIRROR (1)

typedef struct
{
	TIM_HandleTypeDef* timerHandle;
	uint32_t PWMChannel;
	GPIO_TypeDef* DIR_GPIOPort;
	uint16_t DIR_GPIOPin;
	
	__IO int16_t currentAngle;            /* unit: 0.001rad */
	__IO ProjectFloat_t currentAngleF;    /* floating point version of currentAngle */
	int16_t targetAngle;                  /* uint: 0.001rad */
  uint8_t isZeroPointSet;               /* 0 for not set */
	ProjectFloat_t anlgeLimiter_1;        /* the angle of position limiter 1 */
	ProjectFloat_t anlgeLimiter_2;        /* the angle of position limiter 2 */
	uint8_t haveMaxAngle;                 /* 1 for have maximum angle set, 0 for none */
	uint8_t haveMinAngle;                 /* 1 for have minimum angle set, 0 for none */
	int16_t maxAngle;                     /* unit: 0.001rad */
	int16_t minAngle;                     /* unit: 0.001rad */
	ProjectFloat_t anglePerStep;          /* angle per every step, 0.001rad */
	int8_t  direction;                    /* 1 or -1 */
	int8_t  directionMirror;              /* -1 for mirror, 1 for non-mirror */
	ProjectFloat_t directionAnglePerStep; /* the actual angle per step with direction */
	__IO uint16_t reverseCounter;         /* reverse counter for PWM pulses */
	__IO uint8_t upperLimitReached;       /* 1 for reached */
	__IO uint8_t lowerLimitReached;       /* 1 for reached */
	
	uint8_t lock;                         /* 1 for locked */
	__IO uint32_t counter;
	uint8_t isSearchingZeroPoint;         /* 1 for searching, 0 for not searching */
	uint8_t id;                           /* the id number */
	ProjectFlag_t stopRequest;            /* PROJECT_FLAG_SET for stop */
}ProjectStepMotorRepresentation_t;

typedef struct
{
	TIM_HandleTypeDef* hardwareHandle; /* associated hardwre handle */
	GPIO_TypeDef* GPIOPort;
	uint16_t GPIOPin;
	uint16_t GPIOPin_Detect;
	uint32_t distance;                 /* detected distance, uint mm */
	uint16_t preSetDelay;              /* allowed delay per loop, uint: ms */
	uint16_t preSetDelayLoops;         /* total delay loops */
	uint8_t  id;                       /* the id number of the SSONIC */
}ProjectSSONICRepresentation_t;

typedef struct
{
	GPIO_TypeDef* GPIOPort;
	uint16_t GPIOPin;
	IRQn_Type GPIOEXTI_IRQn;
	uint8_t reached;
	uint8_t id;               /* the id number of the MAG */
}ProjectMagSRepresentation_t; /* Magnetic Sensor */

/* Exported constants --------------------------------------------------------*/

#define PROJECT_SPEED_OF_SOUND          (340) /* m/s */
#define PROJECT_SSONIC_DETECT_FREQ  (1000000) /* 1 MHz */
#define PROJECT_SSONIC_DETECT_PERIOD    (100) /* 10 kHz */
#define PROJECT_SSONIC_DETECT_TIMER_FREQ ( PROJECT_SSONIC_DETECT_FREQ / PROJECT_SSONIC_DETECT_PERIOD)

#define PROJECT_NOTHING_IN_PROGRESS (0)
#define PROJECT_SSONIC_IN_PROGRESS  (1)

#define PROJECT_SSONIC_MAX_DELAY (1000) /* uint: ms */
#define PROJECT_SSONIC_MIN_DELAY    (1) /* uint: ms */

#define PROJECT_SSONIC_LOOP_FREQ    (100000) /* 10 kHz */
#define PROJECT_SSONIC_LOOP_PERIOD   (33333) /* 3 Hz */
#define PROJECT_SSONIC_LOOP_TIMER_FREQ (  PROJECT_SSONIC_LOOP_FREQ / PROJECT_SSONIC_LOOP_PERIOD)

#define PROJECT_MAG_SENSOR_REACHED       (1)
#define PROJECT_MAG_SENSOR_NOT_REACHED   (0)

#define PROJECT_LINEAR_VELOCITY_TOP     ( 1000)  /* uint: mm/s */
#define PROJECT_LINEAR_VELOCITY_BOTTOM  (-1000)  /* uint: mm/s */
#define PROJECT_ANGULAR_VELOCITY_TOP     (3140)  /* uint: 0.001 degree/s */
#define PROJECT_ANGULAR_VELOCITY_BOTTOM (-3140)  /* uint: 0.001 degree/s */

typedef enum
{
	PP_SELF_DESCRIPTION                = 0x0001,
	PP_TEST_REGISTER                   = 0x0042,
	PP_ACTION_ENABLE                   = 0x0050,
	PP_ACTION_MOVING_LINEAR_VELOCITY   = 0x0052,
	PP_ACTION_MOVING_ANGULAR_VELOCITY,
	PP_HEAD_MOTOR_GO_TO_ZERO_POINT     = 0x0060,
	PP_HEAD_MOTOR_1_TARGET_ANGLE,
	PP_HEAD_MOTOR_2_TARGET_ANGLE,
	PP_HEAD_MOTOR_1_DELTA_ANGLE,
	PP_HEAD_MOTOR_2_DELTA_ANGLE,
	PP_PRINT_REGISTER_DEFINITION       = 0xF001
}ProjectProtocal_t;

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */

ProjectStatus_t initProjectControl(void);

ProjectStatus_t getNextProjectProtocolString(char* str, uint32_t len, uint32_t* nLeft);

ProjectStatus_t executeCommand(uint8_t func, uint16_t* cmd, uint16_t* data, uint8_t nCmds);

ProjectStatus_t executeWriteFunc(uint16_t* cmd, uint16_t* data, uint8_t nCmds);

ProjectStatus_t executeReadFunc(uint16_t* cmd, uint16_t* data, uint8_t nCmds);

/**
  * result: 0, invalid transfer; 1, valid transfer
  */
ProjectStatus_t checkTransferValidity(ProjectBuffer_t* pbuffer, uint8_t* result);

ProjectStatus_t retriveCommand(ProjectBuffer_t* pbuffer, uint8_t* addr, uint8_t* func, uint16_t* cmd, uint16_t* data, uint8_t nCmds);

ProjectStatus_t actionMove(uint16_t linearV, uint16_t angularV);

ProjectStatus_t actionHead(uint16_t target1, uint16_t target2);
ProjectStatus_t actionHeadDelta(uint16_t target1, uint16_t target2);

ProjectStatus_t actionHeadSearchForZeroPoint(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t calculateLinearAngularVel(uint16_t f1, uint16_t f2, int16_t* lv, int16_t* av);

ProjectStatus_t setPSMR_HEAD1PWM(ProjectStepMotorRepresentation_t* pPSMR);
ProjectStatus_t setPSMR_HEAD2PWM(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t getCurrentAngle_PSMR(ProjectStepMotorRepresentation_t* pPSMR, int16_t* angle);

ProjectStatus_t setUpperLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR);
ProjectStatus_t clearUpperLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t setLowerLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR);
ProjectStatus_t clearLowerLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t isUpperLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t isLowerLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t isEitherLimitReached_PSMR(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t isPSMRValid(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t startPSMR_PWM(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t stopPSMR_PWM(ProjectStepMotorRepresentation_t* pPSMR);

ProjectStatus_t setPSMRTarget(ProjectStepMotorRepresentation_t* pPSMR, int16_t target);

/**
  * a, unit: 0.001rad
	*/
ProjectStatus_t setHEAD1PWMTargetAngle(uint16_t a);

ProjectStatus_t isPSSRValid(ProjectSSONICRepresentation_t* pPSSR);

ProjectStatus_t startSSONIC(ProjectSSONICRepresentation_t* pPSSR);

ProjectStatus_t startSSONIC_DETECT(void);

ProjectStatus_t stopSSONIC_DETECT(void);

ProjectStatus_t startSSONIC_LOOP(void);

ProjectStatus_t stopSSONIC_LOOP(void);

/** 
  * speedOfSound in m/s
  * timerFreq in Hz.
  * Return the distance in milimeter (mm).
	*/
ProjectStatus_t getSSONIC_DETECT_Distance(uint32_t detectCounter, uint32_t timerFreq, uint32_t speedOfSound, uint32_t* distance);

ProjectStatus_t performCallback_SSONIC(ProjectSSONICRepresentation_t* pPSSR);

ProjectStatus_t performCallback_SSONIC_DETECT(TIM_HandleTypeDef *htim);

ProjectStatus_t performCallback_SSONIC_LOOP(TIM_HandleTypeDef *htim);

ProjectStatus_t performCallback_MagSensor(ProjectMagSRepresentation_t* pMSR);

ProjectStatus_t sendCommandToLowLevelHardware(uint8_t addr, uint8_t func, uint16_t cmdTx, uint16_t dataTx, ProjectFlag_t waitFlag, ProjectBuffer_t* projB, uint32_t recLen);

#endif /* __PROJECT_CONTROL_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
