/**
  ******************************************************************************
  * @file    DefMotor.h 
  * @author  MCD Application Team
  * @version V0.1
  * @date    08-December-2015
  * @brief   Header for motor module
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEF_MOTOR_H
#define __DEF_MOTOR_H

/* Includes ------------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

#define DEF_MOTOR_NUM_POLES    (8)
#define DEF_MOTOR_NUM_H        (3) /* number of Hall sensors */
#define DEF_MOTOR_SPEED_RATIO  (10)

#define DEF_MOTOR_DIM_WHEEL    (0.1) /* uint: m */

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */


#endif /* __DEF_MOTOR_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
