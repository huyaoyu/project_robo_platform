/**
  ******************************************************************************
  * @file    Inc/Modebus.h 
  * @author  HU Yaoyu <huyaoyu@sjtu.edu.cn>
  * @version V0.1
  * @date    24-November-2015
  * @brief   Header for Modbus.c module
  ******************************************************************************
  * @attention
  *
	* No attention for now.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MODBUS_H
#define __MODBUS_H

/* Includes ------------------------------------------------------------------*/
#include "CommonHeader.h"
#include "stdint.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

#define MODEBUS_ADDRESS_BYTE_POS             (0)
#define MODEBUS_FUNCTION_BYTE_POS            (1)
#define MODEBUS_REGISTER_BYTE_HIGH_POS       (2)
#define MODEBUS_REGISTER_BYTE_LOW_POS        (3)
#define MODEBUS_WRITE_REGISTER_BYTE_HIGH_POS (4)
#define MODEBUS_WRITE_REGISTER_BYTE_LOW_POS  (5)

#define MODEBUS_WRITE_SIGNLE_FUNC (0x06)
#define MODEBUS_READ_SIGNLE_FUNC  (0x03)

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

unsigned short CRC16(unsigned char *puchMsg,  /* input data to calculate CRC16 */ 
                     unsigned short usDataLen /* number of bytes in puchMsg */ );

int8_t addModbusSuffix(ProjectBuffer_t* buffer, /* the buffer holds the message */
                       uint32_t len     /* the length of the original message */);

#endif /* __MODEBUS_H */

/************************ (C) COPYRIGHT SJTU *****END OF FILE****/
