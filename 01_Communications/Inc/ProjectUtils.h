/**
  ******************************************************************************
  * @file    ProjectUtils.h 
  * @author  MCD Application Team
  * @version V1.2.3
  * @date    09-October-2015
  * @brief   
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __PROJECT_UTILS_H
#define __PROJECT_UTILS_H

/* Includes ------------------------------------------------------------------*/

#include "CommonHeader.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

/**
  * @brief  copy the content between to project buffers
  * @param  from: the source project buffer, to: the destination project buffer. 
  * @note   This function will check the validity and lengths of the project buffers. 
  * @retval State sign. PROJECT_FAIL - error, PROJECT_OK - copy succeed.
  */
ProjectStatus_t copyProjectBuffer(ProjectBuffer_t* from, ProjectBuffer_t* to);

/**
  * @brief  Split one 2 bytes data into two 1 byte data, and store them in the array des.
  * @param  ori: The original data.
	* @param  des: The destination array. 
  * @note   This function will check the validity of des. 
  * @retval State sign. PROJECT_FAIL - error, PROJECT_OK - copy succeed.
  */
ProjectStatus_t splitIntoArray(uint16_t ori, uint8_t* des);

#endif /* __PROJECT_UTILS_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
