/**
  ******************************************************************************
  * @file    Inc/CommonHeader.h 
  * @author  HU Yaoyu <huyaoyu@sjtu.edu.cn>
  * @version V0.1
  * @date    27-November-2015
  * @brief   Common header file.
  ******************************************************************************
  * @attention
  *
	* No attention for now.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __COMMONHEADER_H
#define __COMMONHEADER_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/

#include "stdint.h"
#include <stdio.h>
#include <string.h>

/* Exported types ------------------------------------------------------------*/

typedef struct
{
	uint8_t* buffer;     /* byte buffer */
	uint32_t len;        /* the maximum length of the buffer */
	uint32_t contentLen; /* the length of the content */
}ProjectBuffer_t;

typedef int8_t ProjectStatus_t; /* the status flag */
typedef int8_t ProjectFlag_t;   /* the general flag */
typedef float ProjectFloat_t;   /* the float type */

struct ProjectDebugStringNodeStruct
{
	char* debugString;
	struct ProjectDebugStringNodeStruct* next;
};

typedef struct ProjectDebugStringNodeStruct ProjectDebugStringNode_t;

/* Exported constants --------------------------------------------------------*/

#define PROJECT_FLAG_SET     (1) /* use with ProjectFlag_t */
#define PROJECT_FLAG_NOT_SET (0) /* use with ProjectFlag_t */

/* Exported variables ------------------------------------------------------- */

extern ProjectDebugStringNode_t* projectDSN;  /* head */
extern ProjectDebugStringNode_t* currentPDSN; /* current position */

/* Exported macro ------------------------------------------------------------*/

#ifndef NULL
#define NULL (0)
#endif

#define PROJECT_DEBUG
#define PROJECT_FAIL (-1)
#define PROJECT_OK (1)

#define PROJECT_PI (3.1416)

#define PROJECT_FILL_DEBUG_STRING_NODE(node,str) \
  fillDebugStringNode(node,str,__FILE__,__LINE__,__func__)

/* Exported functions ------------------------------------------------------- */

/**
  * @brief  Check if a project buffer is valid.
  * @param  projBuffer: the project buffer to be checked. 
  * @note   This function will check the validity and lengths of the project buffers. 
  * @retval State sign. PROJECT_FAIL - invalid project buffer, PROJECT_OK - valid.
  */
int8_t isProjectBufferValid(ProjectBuffer_t* projBuffer);

/**
  * @brief  Clear the content of a ProjectBuffer_t buffer. All the byte will be 0x00.
  * @param  projBuffer: the project buffer to be cleared. 
  * @note   This function will check the validity and lengths of the project buffers. 
  * @retval State sign. PROJECT_FAIL - invalid project buffer, PROJECT_OK - valid.
  */
ProjectStatus_t clearProjectBuffer(ProjectBuffer_t* projBuffer);

ProjectStatus_t createDebugStringNode(ProjectDebugStringNode_t** pNodePointer);

/**
  * New memory will be allocated by this function!!
	*/
ProjectStatus_t fillDebugStringNode( ProjectDebugStringNode_t* pNode,
                                       const char* str,
																			 const char* file,
																			 int line,
																			 const char* func);
																			 
ProjectStatus_t appendDebugStringNode(ProjectDebugStringNode_t** current, ProjectDebugStringNode_t* newOne);

ProjectStatus_t releaseDebugStringStack(ProjectDebugStringNode_t** head);

ProjectStatus_t composeDebugString(ProjectDebugStringNode_t* head, char* buffer, uint32_t len);

#ifdef __cplusplus
}
#endif


#endif /* __COMMONHEADER_H */

/************************ (C) COPYRIGHT SJTU *****END OF FILE****/
