/**
  ******************************************************************************
  * @file    DefRobotGeometry.h 
  * @author  MCD Application Team
  * @version V0.1
  * @date    09-December-2015
  * @brief   Header for geometry definition of the robot
  ******************************************************************************
  * @attention
  *
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __DEF_ROBOT_GEOMETRY_H
#define __DEF_ROBOT_GEOMETRY_H

/* Includes ------------------------------------------------------------------*/

/* Exported types ------------------------------------------------------------*/

/* Exported constants --------------------------------------------------------*/

#define DEF_ROBOT_DIS_WHEELS    (250) /* uint: mm */

/* Exported macro ------------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */


#endif /* __DEF_ROBOT_GEOMETRY_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
