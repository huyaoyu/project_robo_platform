/**
  ******************************************************************************
  * @file    Inc/LowLevel_AQMD3605BLS.h 
  * @author  HU Yaoyu <huyaoyu@sjtu.edu.cn>
  * @version V0.1
  * @date    07-December-2015
  * @brief   Header for AQMD3605BLS driver board
  ******************************************************************************
  * @attention
  *
	* No attention for now.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __LOWLEVEL_AQMD3605BLS_H
#define __LOWLEVEL_AQMD3605BLS_H

/* Includes ------------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/

#define LL_MOTOR_ALT_CONST            (20) /* pp. 9 */

#define LL_MOTOR_DRIVER_0              0
#define LL_MOTOR_DRIVER_1              1

#define LL_MD1_ADDR                   (0x01)
#define LL_MD2_ADDR                   (0x02)

#define LL_MD_MODBUS_READ_FUNC_CODE   (0x03)
#define LL_MD_MODBUS_WRITE_FUNC_CODE  (0x06)

#define LL_MD_STOP_REG                (0x0040)
#define LL_MD_PWM_REG                 (0x0042)
#define LL_MD_VEL_CLOSED_LOOP_REG     (0x0043)

/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

#endif /* __LOWLEVEL_AQMD3605BLS_H */

/************************ (C) COPYRIGHT SJTU *****END OF FILE****/
